#!/bin/bash

#Submit script for gbb Tuple Ana
echo "Submit jobs for Charged Higgs Analysis......"

#OUTPATH="/afs/cern.ch/work/s/shbansal/chargedHiggs_Ana/MockOutput"
LOG_FOLDER="/cephfs/user/s6subans/ChargedHiggsLog/"

echo "Logs in: ${LOG_FOLDER}"
echo "Output in: ${OUTPATH}"

OUTDIR="/cephfs/user/s6subans/ChargedHiggs_V2/"
#USE_BATCH_MODE=1

TOPCON=(
#"250"
"225"
#"250"
#"275"
#"300"
#"325"
#"350"   
)

WP=(
#"77p"
"77p"
#"85p"
#"60p"
)

export EOS_MGM_URL=root://eosuser.cern.ch
echo "reading files !!! "
Paths=(  
"ChargedHiggsNtups/HpWh_Ntup_2023/MC16a/"
"ChargedHiggsNtups/HpWh_Ntup_2023/MC16d/"
"ChargedHiggsNtups/HpWh_Ntup_2023/MC16e/"
# "ChargedHiggsNtups/HpWh_Ntup_2021/MC16a/"
# "ChargedHiggsNtups/HpWh_Ntup_2021/MC16d/"
# "ChargedHiggsNtups/HpWh_Ntup_2021/MC16e/"
##"ChargedHiggsNtups/HpWh_Ntup_2021/"
#"ChargedHiggsNtups/HpWh_Ntup_2023/data/"
#"ChargedHiggsNtups/HpWh_Ntup_2021/data_V2/"
#"/afs/cern.ch/work/s/shbansal/chargedHiggs_Ana/chargedHiggs_MC16a/"
)

File=(
364250
364253
364254
364255
364288
364289
364290
363355
363356
363357
363358
363359
363360
363489
363494
364283
364284
364285
364287
345705
345706
345723
# 364170
# 364171
# 364172
# 364173
# 364174
# 364175
# 364176
# 364177
# 364178
# 364179
# 364180
# 364181
# 364182
# 364183
# 364156
# 364157
# 364158
# 364159
# 364160
# 364161
# 364162
# 364163
# 364164
# 364165
# 364166
# 364167
# 364168
# 364169
# 364184
# 364185
# 364186
# 364187
# 364188
# 364189
# 364190
# 364191
# 364192
# 364193
# 364194
# 364195
# 364196
# 364197
# 364114
504555
504556
504557
504558
504559
504560
504561
504562
504563
504564
504565
504566
504567
504568
504569
504570
504571
510115
510116
510117
510118
510119
510120
510121
510122
510123
510124
#510214
#510215
#510216
# 364115
# 364116
# 364117
# 364118
# 364119
# 364120
# 364121
# 364122
# 364123
# 364124
# 364125
# 364126
# 364127
# 364100
# 364101
# 364102
# 364103
# 364104
# 364105
# 364106
# 364107
# 364108
# 364109
# 364110
# 364111
# 364112
# 364113
# 364128
# 364129
# 364130
# 364131
# 364132
# 364133
# 364134
# 364135
# 364136
# 364137
# 364138
# 364139
# 364140
# 364141
# 364204
# 364205
# 364206
# 364207
# 364208
# 364209
# 364198
# 364199
# 364200
# 364201
# 364202
# 364203
# 364210
# 364211
# 364212
# 364213
# 364214
# 364215
700320
700321
700322
700323
700324
700325
700326
700327
700328
700329
700330
700331
700332
700333
700334
700338
700339
700340
700341
700342
700343
700344
700345
700346
700347
700348
700349
#zjets.root   
#st_sc.root   
#st_tc.root   
#tH_AFII.root     
#tt_PP8.root
410470
411073
411074
411075
411076
411077
411078
# 2015
# 2016
# 2017
# 2018
346343
346344
346345
410408
346676
346678
410560
410644
410645
410646
410647
410658
410659
413008
413023
412043
# st_tc.root
# st_sc.root
# tH_AFII.root
# ttH_PP8.root
# ttll.root
# tWZ.root
# tZ.root
# Wt.root
407342
407343
407344
#tWZ.root         
#tt_PP8filtered.root
#tZ.root          
#ttll.root
#ttH_PP8.root         
#wjets.root
#ttW.root             
#db.root    
#Wt.root   
# hp250_AFII.root
# hp300_AFII.root
# hp350_AFII.root
#hp400_AFII.root
# hp500_AFII.root
# hp600_AFII.root
# hp700_AFII.root
# hp800_AFII.root
# hp900_AFII.root
#hp1000_AFII.root
# hp1200_AFII.root
# hp1400_AFII.root
# hp1600_AFII.root
# hp1800_AFII.root
#hp2000_AFII.root
# hp2500_AFII.root
# hp3000_AFII.root
#tt_4FS.root
#tt_H7.root
#tt_NLO.root
# data_2015.root
# data_2016.root
# data_2017.root
# data_2018.root
)

#rm -rf cluster_pack.tar.gz
#tar -czf cluster_pack.tar.gz Makefile_batch  main_RunMVATraining.C dataset/ main/ TH1Fs/ utilis/ LatexOutput/ python/ style/

echo "sucessfuly opened tar files"
for topc in "${TOPCON[@]}"
do
   for path in "${Paths[@]}"
   do
      for file in "${File[@]}"
      do
      ##./execute $path $file $wp $OUTDIR $USE_BATCH_MODE
      condor_submit IN_PATH="${path}" FILE="${file}" WP="${WP}" OUTDIR="${OUTDIR}" TOPCON="${topc}" /cephfs/user/s6subans/ChargedHiggsAna_V2L2/Code_Boosted/run_Hplus.sub
      done
   done
done

echo "all done !!! "