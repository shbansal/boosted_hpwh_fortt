# -*- coding: utf-8 -*-
#python
import sys
import glob
import math
import re
from ROOT import *
from array import *
from ROOT import TCanvas, TFile, TLine, TProfile, TNtuple, TH1F, TH2F
#import ROOT
#import array

gROOT.SetBatch(True)
gStyle.SetOptStat(0)
gStyle.SetPalette(1)
gROOT.LoadMacro("../style/AtlasStyle.C")
gROOT.LoadMacro("../style/AtlasUtils.C")
SetAtlasStyle()

y_axis_label="Event fraction"

c_blue   = TColor.GetColor("#3366ff")
c_red    = TColor.GetColor("#ee3311")
c_orange = TColor.GetColor("#ff9900")

def NormalizeHisto(histo):
     #n_events=histo.Integral(-1,histo.GetNbinsX()+1)
     n_events=histo.Integral()
     if n_events == 0:
         return
     #print n_events, histo.Integral(histo.GetNbinsX(),histo.GetNbinsX()+1)
     histo.Scale(1./n_events)
     histo.SetLineWidth(3)
     histo.SetStats(0)
     histo.SetFillStyle(3001)
     histo.SetMarkerColor(histo.GetLineColor())
     histo.SetMarkerSize(0.0)
     histo.GetXaxis().SetTitleOffset(1.2)
     histo.GetYaxis().SetTitleOffset(1.52)
     histo.GetXaxis().SetLabelSize(0.05)
     histo.GetYaxis().SetLabelSize(0.05)
     histo.GetXaxis().SetTitleSize(0.05)
     histo.GetYaxis().SetTitleSize(0.05)
     histo.GetYaxis().SetNdivisions(504)
     histo.GetXaxis().SetNdivisions(504)

#c1 = TCanvas("ShapePlots","",500,500)

c1 = TCanvas("ShapePlots","",700,500)
pad1 = TPad("pad1", "pad1", 0.0, 0.35, 1,1)
#pad1.SetLogy()
pad2 = TPad("pad2", "pad2", 0, 0.0, 1,0.35)
pad1.SetLeftMargin(0.15)
pad1.SetTopMargin(0.1)
pad1.SetBottomMargin(0.0105)
pad1.SetRightMargin(0.08)
pad1.Draw()
pad1.SetTicks()

pad2.SetLeftMargin(0.15)
pad2.SetTopMargin(0.04)
pad2.SetBottomMargin(0.35)
pad2.SetRightMargin(0.08)
pad2.Draw()
pad2.SetTicks()

#HistoNameR = "Hadronic_Top"
HistoNameR = "Leptonic_Top_mass_minXwT"
#for HistoName in ["nBTags"]:
#for HistoName in ["nBTags","nJets","HT","HT_bjets","DeltaPhi_HW","mVH","mH","pTWplus","pTH","maxMVAResponse", "mass_resolution"]: # for resolved, histoname set
#for HistoName in ["mVH","pTWplus","pTH","mH","pTWminus"]: # for resolved, histoname set
#for HistoName in ["nBTags","nJets","HT","HT_bjets","DeltaPhi_HW","mVH","mH","pTWplus","pTH","mass_resolution"]:#for boosted, histoname set
#for HistoName in ["mass_resolution"]:
#for HistoName in ["mVH","MET","Lepton_Eta","Lepton_Pt","Muon_Eta","Muon_Pt","nJet","Mwt","HT_all","mVH","DeltaPhi_HW","DeltaEta_HW","maxMVAResponse"]:   
#for HistoName in ["lep_top_mass_minXwT"]:  
for HistoName in ["HT"]:
#for HistoName in ["maxMVAResponse"]:
## for bTagStrategy in ["Incl","FourPlusTags","ThreeTags","TwoTags"]:
 for Region in ["Merged_SR"]:
    for btagStrategy in ["Inclusive"]:
    #for btagStrategy in ["Inclusive","FourPlusTags","ThreeTags","TwoTags"]:
     
      ReBin = False
      YAxisScale = 1.4

      if "nBTags" in HistoName:
          Xaxis_label="b-tag multiplicity"
      if "pTWminus" in HistoName:
          Xaxis_label="Transverse Momentum Of Wminus [GeV]"
      if "nJet" in HistoName:
          Xaxis_label="Jet Multiplicity"
      if "mWplus" in HistoName:
          Xaxis_label="Mass of Wplus [GeV]"
      if "mH" in HistoName:
          Xaxis_label="Mass of Higgs [GeV]"
      if "DeltaPhi_HW" in HistoName:
          Xaxis_label="DeltaPhi_HW"
      if "pTH" in HistoName:
          Xaxis_label="Transverse Momentum of Higgs [GeV]"
      if "pTWplus" in HistoName:
          Xaxis_label="Transverse Momentum of W Boson [GeV]"
      if "mVH" in HistoName:
          Xaxis_label="Mass of charged Higgs [TeV]"
      if "mH" in HistoName:
          Xaxis_label="Mass of Higgs [GeV]"
      if "pTWplus" in HistoName:
          Xaxis_label="Transverse Momentum Of Wplus [GeV]"
      if "pTH" in HistoName:
          Xaxis_label="Transverse Momentum Of Higgs [GeV]"
      if "mass_resolution" in HistoName:
          Xaxis_label="Mass Resolution"
      if "HT_all" in HistoName:
          Xaxis_label="H_{T_{all}} [GeV]"
      if "HT" in HistoName:
          Xaxis_label="Transverse momentum sum of large-R jets [GeV]"    
      if "HT_bjets" in HistoName:
          Xaxis_label="H_{T_{b-jet}} (Scalar Transverse Momentum Sum of b-jets) [GeV]"
      if "Lepton_Pt" in HistoName:
          Xaxis_label="Transverse momentum of lepton [GeV]" 
      if "Lepton_Eta" in HistoName:
          Xaxis_label="Lepton \eta"   
      if "Muon_Pt" in HistoName:
          Xaxis_label="Transverse momentum of muon [GeV]" 
      if "Muon_Eta" in HistoName:
          Xaxis_label="Muon \eta" 
      if "MET" in HistoName:
          Xaxis_label="Missing transverse energy [GeV]"      
      if "Mwt" in HistoName:
          Xaxis_label="Transverse mass of W boson [GeV]"
      if "DeltaPhi_HW" in HistoName:
          Xaxis_label="\Delta_{\Phi_{HW}}"
      if "DeltaEta_HW" in HistoName:
          Xaxis_label="\Delta_{\Eta_{HW}}"    
      if "maxMVAResponse" in HistoName:
          Xaxis_label="BDT Score (Signal Reconstruction)"
      if "mass_resolution" in HistoName:
          Xaxis_label="Leptonic Top [GeV]"   
      if "lep_top_mass_minXwT" in HistoName:
          Xaxis_label="Leptonic top mass [GeV]"
      #Xaxis_label=""
      
      file_tt   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Boosted_tt_Sel1/tt_boosted_de.root","READ")
      dir_tt_1b   = file_tt.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1b")
      h_ttbar_background_1b = dir_tt_1b.Get("tt_PP8_1B_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_ttbar_background_1b = dir_tt_1b.Get("tt_PP8_"+HistoName+"_1b_"+Region+"_"+btagStrategy+"_nominal_Loose")
      dir_tt_1c   = file_tt.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1c")
      h_ttbar_background_1c = dir_tt_1c.Get("tt_PP8_1C_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_ttbar_background_1c = dir_tt_1c.Get("tt_PP8_"+HistoName+"_1c_"+Region+"_"+btagStrategy+"_nominal_Loose")
      dir_tt_1l   = file_tt.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1l")
      h_ttbar_background_1l = dir_tt_1l.Get("tt_PP8_1L_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_ttbar_background_1l = dir_tt_1l.Get("tt_PP8_"+HistoName+"_1l_"+Region+"_"+btagStrategy+"_nominal_Loose")
      
      file_tt_filt   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Boosted_tt_Sel1/tt_boosted_de.root","READ")
      dir_tt_1b_filt   = file_tt_filt.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1b")
      h_ttbar_background_1b_filt = dir_tt_1b_filt.Get("tt_PP8filtered_1B_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_ttbar_background_1b_filt = dir_tt_1b_filt.Get("tt_PP8filtered_"+HistoName+"_1b_"+Region+"_"+btagStrategy+"_nominal_Loose")
      dir_tt_1c_filt   = file_tt_filt.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1c")
      h_ttbar_background_1c_filt = dir_tt_1c_filt.Get("tt_PP8filtered_1C_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_ttbar_background_1c_filt = dir_tt_1c_filt.Get("tt_PP8filtered_"+HistoName+"_1c_"+Region+"_"+btagStrategy+"_nominal_Loose")
      dir_tt_1l_filt   = file_tt_filt.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1l")
      h_ttbar_background_1l_filt = dir_tt_1l_filt.Get("tt_PP8filtered_1L_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_ttbar_background_1l_filt = dir_tt_1l_filt.Get("tt_PP8filtered_"+HistoName+"_1l_"+Region+"_"+btagStrategy+"_nominal_Loose")

    
      h_ttbar_background_1l = h_ttbar_background_1l + h_ttbar_background_1l_filt
      h_ttbar_background_1c = h_ttbar_background_1c + h_ttbar_background_1c_filt
      h_ttbar_background_1b = h_ttbar_background_1b + h_ttbar_background_1b_filt      

      h_ttbar_background = h_ttbar_background_1b + h_ttbar_background_1c + h_ttbar_background_1l     

      file_tt_II   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Boosted_tt_Sel2/tt_boosted_de.root","READ")
      dir_tt_1b_II   = file_tt_II.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1b")
      h_ttbar_background_1b_II = dir_tt_1b_II.Get("tt_PP8_1B_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_ttbar_background_1b_II = dir_tt_1b_II.Get("tt_PP8_"+HistoName+"_1b_"+Region+"_"+btagStrategy+"_nominal_Loose")
      dir_tt_1c_II   = file_tt_II.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1c")
      h_ttbar_background_1c_II = dir_tt_1c_II.Get("tt_PP8_1C_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_ttbar_background_1c_II = dir_tt_1c_II.Get("tt_PP8_"+HistoName+"_1c_"+Region+"_"+btagStrategy+"_nominal_Loose")
      dir_tt_1l_II   = file_tt_II.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1l")
      h_ttbar_background_1l_II = dir_tt_1l_II.Get("tt_PP8_1L_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_ttbar_background_1l_II = dir_tt_1l_II.Get("tt_PP8_"+HistoName+"_1l_"+Region+"_"+btagStrategy+"_nominal_Loose")
      
      file_tt_filt_II   = TFile.Open("/cephfs/user/s6subans/ChargedHiggs_V2/Boosted_tt_Sel2/tt_boosted_de.root","READ")
      dir_tt_1b_filt_II   = file_tt_filt_II.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1b")
      h_ttbar_background_1b_filt_II = dir_tt_1b_filt_II.Get("tt_PP8filtered_1B_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_ttbar_background_1b_filt_II = dir_tt_1b_filt_II.Get("tt_PP8filtered_"+HistoName+"_1b_"+Region+"_"+btagStrategy+"_nominal_Loose")
      dir_tt_1c_filt_II   = file_tt_filt_II.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1c")
      h_ttbar_background_1c_filt_II = dir_tt_1c_filt_II.Get("tt_PP8filtered_1C_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_ttbar_background_1c_filt_II = dir_tt_1c_filt_II.Get("tt_PP8filtered_"+HistoName+"_1c_"+Region+"_"+btagStrategy+"_nominal_Loose")
      dir_tt_1l_filt_II   = file_tt_filt_II.GetDirectory("nominal_Loose").GetDirectory(HistoName+"_1l")
      h_ttbar_background_1l_filt_II = dir_tt_1l_filt_II.Get("tt_PP8filtered_1L_"+HistoName+"_"+Region+"_"+btagStrategy+"_nominal_Loose")
      #h_ttbar_background_1l_filt_II = dir_tt_1l_filt_II.Get("tt_PP8filtered_"+HistoName+"_1l_"+Region+"_"+btagStrategy+"_nominal_Loose")

    
      h_ttbar_background_1l_II = h_ttbar_background_1l_II + h_ttbar_background_1l_filt_II
      h_ttbar_background_1c_II = h_ttbar_background_1c_II + h_ttbar_background_1c_filt_II
      h_ttbar_background_1b_II = h_ttbar_background_1b_II + h_ttbar_background_1b_filt_II      

      h_ttbar_background_II = h_ttbar_background_1b_II + h_ttbar_background_1c_II + h_ttbar_background_1l_II  
        
      h_ttbar_background.SetLineColor(kRed)
      h_ttbar_background.SetLineStyle(7) #7

      h_ttbar_background_II.SetLineColor(kBlue)
      h_ttbar_background_II.SetLineStyle(7) #7

      print h_ttbar_background.Integral()
      print h_ttbar_background_II.Integral()

      nbins=20
      ymax=0
      #NormalizeHisto(h_ttbar_background)
      if ymax<h_ttbar_background.GetMaximum():
          ymax=h_ttbar_background.GetMaximum()
      #NormalizeHisto(h_ttbar_background_II)
      if ymax<h_ttbar_background_II.GetMaximum():
          ymax=h_ttbar_background_II.GetMaximum()
      pad1.cd()    
      h_ttbar_background.Draw("HIST")
      #h_sig_Hplus_m300.Draw("HISTSAME")
      #h_sig_Hplus_m350.Draw("HISTSAME")
      h_ttbar_background_II.Draw("HISTSAME")
      #h_sig_Hplus_m500.Draw("HISTSAME")
      #h_sig_Hplus_m600.Draw("HISTSAME")
      #h_sig_Hplus_m700.Draw("HISTSAME")
      #h_sig_Hplus_m400III.Draw("HISTSAME")
      #h_sig_Hplus_m1400.Draw("HISTSAME")
     
      if HistoName in "maxMVAResponse":
         leg = TLegend(0.25,0.55,0.825,0.755)
      else:
         leg = TLegend(0.43,0.65,0.68,0.85)
      #ATLAS_LABEL(0.20,0.875,"Simulation",1,0.19);
      #ATLAS_LABEL(0.20,0.80,"Internal",1,0.09)
      leg.SetShadowColor(kWhite)
      leg.SetFillColor(kWhite)
      leg.SetLineColor(kWhite)
      
      leg.AddEntry(h_ttbar_background,    "Selection 1","L")
      #leg.AddEntry(h_sig_Hplus_m300,    "H^{+}#rightarrow W^{+}h(m_{H^{+}} = 300GeV)","L")
      #leg.AddEntry(h_sig_Hplus_m350,   "H^{+}#rightarrow W^{+}h(m_{H^{+}} = 350GeV)","L")
      leg.AddEntry(h_ttbar_background_II,    "Selection 2","L")
      #leg.AddEntry(h_sig_Hplus_m400III,    "Inc, m_{t_{l}}<=225 GeV","L")
      #leg.AddEntry(h_sig_Hplus_m400III,    "Training with dR(jj)>1.0, Inc, m_{t_{l}}<=225 GeV","L")
      #leg.AddEntry(h_sig_Hplus_m400IV,    "Inc, m_{t_{l}}<=250 GeV","L")
      #leg.AddEntry(h_sig_Hplus_m400V,    "Inc, m_{t_{l}}<=275 GeV","L")
      #leg.AddEntry(h_sig_Hplus_m400VI,    "Inc, m_{t_{l}}<=300 GeV","L")
      #leg.AddEntry(h_sig_Hplus_m400VII,    "Inc, m_{t_{l}}<=325 GeV","L")
      #leg.AddEntry(h_sig_Hplus_m400VIII,    "Inc, m_{t_{l}}<=350 GeV","L")
      
      leg.SetTextSize(0.0250)
      leg.Draw()
    
      pad2.SetGrid()
      pad2.cd()
      h_ratio1 = h_ttbar_background.Clone()
      #h_ratio2 = h_ttbar_background_1c.Clone()
      h_ratio1.Divide(h_ttbar_background_II)
      #h_ratio2.Divide(h_ttbar_background_1l)
      if "TwoTags" in btagStrategy:
        h_ratio1.GetYaxis().SetRangeUser(0.8,1.2) 
      if "ThreeTags" in btagStrategy :
        h_ratio1.GetYaxis().SetRangeUser(0.8,1.2)
      if "FourPlusTags" in btagStrategy :
        h_ratio1.GetYaxis().SetRangeUser(0.8,1.2)    

      h_ratio1.SetStats(0)
      h_ratio1.SetMarkerStyle(20)
      h_ratio1.SetMarkerSize(1.0)
      h_ratio1.SetMarkerColor(kBlack)
      
      print h_ttbar_background.Integral()
      print h_ttbar_background_II.Integral()

      h_ratio1.SetLabelSize(0.082,"X")
      h_ratio1.SetLabelSize(0.082,"Y")
      h_ratio1.SetTitleSize(0.082,"X")
      h_ratio1.SetTitleSize(0.082,"Y")
      h_ratio1.SetTitleOffset(0.48,"Y")
      h_ratio1.SetTitleOffset(1.2,"X")
      h_ratio1.GetYaxis().SetNdivisions(505)
      h_ratio1.SetTickLength(0.005,"X") #0.05
      h_ratio1.SetLabelOffset(0.01,"X")
      h_ratio1.GetXaxis().SetTitle(Xaxis_label)
      h_ratio1.GetYaxis().SetTitle("Sel1/Sel2")
      h_ratio1.Draw("EP")
      
      pad1.cd()
      myText(0.55,0.60,1,"#sqrt{s}=13 TeV, 103.42 fb^{-1}")
      if "TwoTags" in btagStrategy:
          myText(0.55,0.55,1,"2 b-tag")
      if "ThreeTags" in btagStrategy :
          myText(0.55,0.55,1,"3 b-tag")
          #myText(0.20,0.65,1,"CR cuts")
      if "FourPlusTags" in btagStrategy :
          myText(0.55,0.55,1,"at least 4 b-tag")
          #myText(0.20,0.65,1,"CR cuts")
      if "Inclusive" in btagStrategy :  
          myText(0.55,0.55,1,"l+jets Boosted: Inclusive")
      h_ttbar_background.GetXaxis().SetTitle(Xaxis_label)
      h_ttbar_background.GetYaxis().SetRangeUser(0.001,round(ymax*1.25,3)+0.001)
      h_ttbar_background.GetYaxis().SetTitle("Entries")
      #h_sig_Hplus_m400.GetXaxis().SetTitle(Xaxis_label)
      #h_sig_Hplus_m400.GetYaxis().SetRangeUser(0.001,round(ymax*1.25,3)+0.001)
      #pad1.cd()
      #myText(0.20,0.825,1,"at least 5 jet, 2 b-tags")
      c1.RedrawAxis()
      c1.Update()
      c1.RedrawAxis()
      c1.SaveAs("../Plots/Plot_%s_qqbb_PS_Boosted_Sel1_vs_Sel2.pdf" % (HistoName+"_"+btagStrategy))
      c1.SaveAs("../Plots/Plot_%s_qqbb_PS_Boosted_Sel1_vs_Sel2.png" % (HistoName+"_"+btagStrategy))