//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Fri Nov 15 13:27:20 2019 by ROOT version 6.18/00
// from TTree Nominal/Nominal
// found on file: ../my_analysis_dir/run/submitDir_HIGG5D2_1L-MC16a-HVT-32-14-TCC-Wplus-2019-11-15_12h09/data-EasyTree/ttbar_nonallhad_PwPy8.root
//////////////////////////////////////////////////////////

#ifndef EventLoop2Lep_h
#define EventLoop2Lep_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "string"
#include "TLorentzVector.h"
#include "vector"
#include <iostream>

#include "Chi2_minimization.h"	
#include "TLorentzVector.h"
#include "TH1F.h"

#include "TH1Fs/TH1Fs.h"

class EventLoop_2Lep {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.
   
   // Declaration of leaf types
   
   bool            isMu;
   bool            isE;
   bool            isEMu;
   Int_t           nJet;
   Int_t           nTags;
   Int_t           nFatJets;
   Int_t           nTrkJets;
   Int_t           RunNumber;
   Int_t           nBTrkJets;
   Int_t           nTrackJets;
   Int_t           EventNumber;
   Int_t           passedTauVeto;
   Int_t           passedTrigger;
   Int_t           MCChannelNumber;
   Int_t           nbTagsOutsideFJ;
   Float_t         lep1charge;
   Float_t         lep2charge;
   Float_t         flavL1;
   Float_t         flavL2;
   Int_t           isMediumL1;
   Int_t           isMediumL2;
   Float_t         mLL;
   Float_t         ptL1;
   Float_t         ptL2;
   Float_t         etaL1;
   Float_t         etaL2;
   Float_t         ActualMu;
   Float_t         EventWeight;
   std::string          *Description;
   TLorentzVector  *MET;
   TLorentzVector  *Lep1p4;
   TLorentzVector  *Lep2p4;
   TLorentzVector  *VBFjet1_merged;
   TLorentzVector  *VBFjet2_merged;
   TLorentzVector  *VBFjet1_resolved;
   TLorentzVector  *VBFjet2_resolved;
   std::vector<float>   *FatJet_M;
   std::vector<float>   *FatJet_PT;
   std::vector<float>   *FatJet_Eta;
   std::vector<float>   *FatJet_Phi;
   std::vector<float>   *signal_Jet_M;
   std::vector<float>   *forward_Jet_M;
   std::vector<float>   *signal_Jet_PT;
   std::vector<float>   *forward_Jet_PT;
   std::vector<float>   *signal_Jet_Eta;
   std::vector<float>   *signal_Jet_Phi;
   std::vector<float>   *forward_Jet_Eta;
   std::vector<float>   *forward_Jet_Phi;
   std::vector<float>   *btag_score_selectJet;
   std::vector<float>   *btag_score_signalJet;
   std::vector<float>   *btag_score_forwardJet;
   std::vector<float>   *nbTagsInFJ;
   std::vector<float>   *nbTrkjetsInFJ;
   std::vector<float>   *TrackJet_PT;
   std::vector<float>   *TrackJet_Eta;
   std::vector<float>   *TrackJet_Phi;
   std::vector<float>   *TrackJet_M;
   std::vector<float>   *TrackJet_btagWeight;
   std::vector<TLorentzVector*> NeutrinoVec1;
   std::vector<TLorentzVector*> NeutrinoVec2;
   map<TString, bool> pass_sel;

   // List of branches
   TBranch        *b_nJet;   //!
   TBranch        *b_nTags;  //!
   TBranch        *b_nFatJets;   //!
   TBranch        *b_nTrkJets;   //!
   TBranch        *b_RunNumber;   //!
   TBranch        *b_nBTrkJets;  //!
   TBranch        *b_nTrackJets;  //!
   TBranch        *b_EventNumber; //!
   TBranch        *b_passedTauVeto; //!
   TBranch        *b_passedTrigger; //!
   TBranch        *b_MCChannelNumber; //!
   TBranch        *b_nbTagsOutsideFJ; //!
   TBranch        *b_lep1charge;  //!
   TBranch        *b_lep2charge; //!
   TBranch        *b_flavL1;  //!
   TBranch        *b_flavL2;  //!
   TBranch        *b_isMediumL1;  //!
   TBranch        *b_isMediumL2;  //!
   TBranch        *b_mLL; //!
   TBranch        *b_ptL1;   //!
   TBranch        *b_ptL2;  //!
   TBranch        *b_etaL1;  //!
   TBranch        *b_etaL2;  //!
   TBranch        *b_ActualMu;   //!
   TBranch        *b_EventWeight;   //!
   TBranch        *b_Description;   //!
   TBranch        *b_MET;   //!
   TBranch        *b_Lep1p4;  //!
   TBranch        *b_Lep2p4;  //!
   TBranch        *b_VBFjet1_merged; //!
   TBranch        *b_VBFjet2_merged; //!
   TBranch        *b_VBFjet1_resolved; //!
   TBranch        *b_VBFjet2_resolved; //!
   TBranch        *b_FatJet_M;   //!
   TBranch        *b_FatJet_PT;   //!
   TBranch        *b_FatJet_Eta;   //!
   TBranch        *b_FatJet_Phi;   //!
   TBranch        *b_signal_Jet_M;   //!
   TBranch        *b_forward_Jet_M;   //!
   TBranch        *b_signal_Jet_PT;   //!
   TBranch        *b_forward_Jet_PT;   //!
   TBranch        *b_signal_Jet_Eta;   //!
   TBranch        *b_signal_Jet_Phi;   //!
   TBranch        *b_forward_Jet_Eta;   //!
   TBranch        *b_forward_Jet_Phi;   //!
   TBranch        *b_btag_score_selectJet;   //!
   TBranch        *b_btag_score_signalJet;   //!
   TBranch        *b_btag_score_forwardJet;   //!
   TBranch        *b_TrackJet_PT;   //!
   TBranch        *b_TrackJet_Eta;   //!
   TBranch        *b_TrackJet_Phi;   //!
   TBranch        *b_TrackJet_M;   //!
   TBranch        *b_TrackJet_btagWeight;   //!

   EventLoop_2Lep(TTree *tree=0, TString sampleName="", TString ExpUncertaintyName="Nominal");
   void Write(TFile *outfile);
   void Write(TDirectory *dir, std::string dirname);
   bool PassEventSelection2Lep();
   virtual ~EventLoop_2Lep();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree, TString sampleName, TString ExpUncertaintyName);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);

   
   TH1F *Chi2HadH_negLepton;
   TH1F *Chi2LepT_negLepton;
   TH1F *Fjet_maxM;
   TH1F *Fjet_minM;
   TH1F *Fjet_M;
   TH1F *LeadingFJets_DPhi;
   TH1F *LeadingFJets_M;
   TH1F *NBJets;
   TH1F *Chi2HadT_negLepton;
   TH1F *Mwt;
   TH1F *Leptonic_Reco_W_Pt;
   TH1F *Leptonic_Reco_W_M;
   TH1F *Leptonic_Reco_W_Eta;
   TH1F *Leptonic_Reco_chH_M;
   TH1F *Leptonic_Reco_chH_Pt;
   TH1F *Leptonic_Reco_chH_Eta;
   TH1F *DPhi_LepW_chH;
   TH1F *minDeltaR;
   TH1F *NLargeRJets;
   TH1F *overlap_Jets_negLep;
   TH1F *NBtags_TrackJ;
   TH1F *NTrackJets;
   TH1F *DPhi_HW_leptonicDecay;
   TH1F *DPhi_HW_hadronicDecay;
   TH1F *M_WH_leptonic;
   TH1F *M_WH_hadronic;
   TH1F *M_leadingFJet_hadronic;
   TH1F *M_secondleadingFJet_hadronic;
   TH1F *M_leadingFJet_leptonic;

   TH1Fs *h_M_WH;
   TH1Fs *h_MET;
   TH1Fs *h_Mwt;
   TH1Fs *h_Lepton_Eta;
   TH1Fs *h_Lepton_Pt;
   TH1Fs *h_FJet1_Pt;
   TH1Fs *h_FJet2_Pt;
   TH1Fs *h_Fjet1_M;
   TH1Fs *h_Fjet2_M;
   TH1Fs *h_FJet1_Eta;
   TH1Fs *h_FJet2_Eta;
   TH1Fs *h_DeltaPhi_FJ;
   TH1Fs *h_NBtags;
   TH1Fs *h_HiggsCandidate_merged;
   TH1Fs *h_WBosonCandidate_merged;
   TH1Fs *h_DeltaPhi_lepT_hadchH;
   TH1Fs *h_chi2ming1L_lepT;
   TH1Fs *h_lepT_M;
   TH1Fs *h_Chi2LepT_negLepton;
   TH1Fs *h_posLep_chH_M;
   TH1Fs *h_posLep_chH_Pt;
   TH1Fs *h_posLep_chH_Eta;
   TH1Fs *h_posLep_higgsJet_Pt;
   TH1Fs *h_posLep_higgsJet_M;
   TH1Fs *h_posLep_higgsJet_Eta;
   TH1Fs *h_posLep_WBoson_Pt;
   TH1Fs *h_posLep_WBoson_Eta;
   TH1Fs *h_posLep_DeltaPhi_W_h;
   TH1Fs *h_posLep_hadTop_Pt;
   TH1Fs *h_posLep_hadTop_Eta;
   TH1Fs *h_posLep_hadTop_M;
   TH1Fs *h_TrackJet_PT;
   TH1Fs *h_TrackJet_Eta;
   TH1Fs *h_TrackJet_Phi;
   TH1Fs *h_TRackJet_M;
   TH1Fs *h_NBtags_TrackJ;
   TH1Fs *h_NmatchVR_HJ;
   TH1Fs *h_DeltaR_VRJ;
   
   vector<double>  m_EventWeights;
   vector<TString> m_UncNames;
   vector<TString> mySel;
   
   TLorentzVector HiggsCandidate_merged;
   TLorentzVector WBosonCandidate_merged;
   TLorentzVector hadronicTop;
   TLorentzVector leptonicTop;
   TLorentzVector hadronicchH_smallRJ;
   TLorentzVector chargedHiggs;
   TLorentzVector fjet1;
   TLorentzVector fjet2;
   std::vector<TLorentzVector> Jets;
   std::vector<TLorentzVector*> Jets_pointer;
   std::vector<TLorentzVector> BJets;
   std::vector<TLorentzVector> FJets;
   std::vector<TLorentzVector> TrJets;
   std::vector<TLorentzVector> BTrJets;
   std::vector<TLorentzVector> HiggsCandidate_VRJs;
   double chi2_lepT;
   double chi2_hadT;
   int NBJet;
   int NTags;
   int NBTrJet;
};

#endif

#ifdef EventLoop2Lep_cxx
EventLoop_2Lep::EventLoop_2Lep(TTree *tree, TString sampleName, TString ExpUncertaintyName) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
	std::cerr << "Error in EventLoop::EventLoop(): tree is nullptr" << std::endl;
	return;
   }
   Init(tree, sampleName, ExpUncertaintyName);
}

EventLoop_2Lep::~EventLoop_2Lep()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t EventLoop_2Lep::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t EventLoop_2Lep::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void EventLoop_2Lep::Init(TTree *tree, TString sampleName, TString ExpUncertaintyName)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).
   
   m_UncNames      = {""};
   
   ////mySel = {"QuasiIncl_HasTauTag", "VBF_HasTauTag", "4jplus_HasTauTag", "incl_HasTauTag",
   mySel ={ "mbbCR1", "SR1","mbbCR2", "SR2"};
   
   h_M_WH                   = new TH1Fs(sampleName+"_M_WH", "", 25, 500, 2000, mySel, m_UncNames, ExpUncertaintyName);
   h_MET                    = new TH1Fs(sampleName+"_MET", "", 50, 0, 600, mySel, m_UncNames, ExpUncertaintyName);
   h_Mwt                    = new TH1Fs(sampleName+"_Mwt", "", 50, 0, 300, mySel, m_UncNames, ExpUncertaintyName);
   h_Lepton_Eta             = new TH1Fs(sampleName+"_Lepton_Eta", "", 50, -5, 5, mySel, m_UncNames, ExpUncertaintyName);
   h_Lepton_Pt              = new TH1Fs(sampleName+"_Lepton_Pt", "", 50, 0, 600, mySel, m_UncNames, ExpUncertaintyName);
   h_FJet1_Pt               = new TH1Fs(sampleName+"_FJet1_Pt", "", 50, 0, 1200, mySel, m_UncNames, ExpUncertaintyName);
   h_FJet2_Pt               = new TH1Fs(sampleName+"_FJet2_Pt", "", 50, 0, 700, mySel, m_UncNames, ExpUncertaintyName);
   h_Fjet1_M                = new TH1Fs(sampleName+"_Fjet1_M", "", 50, 0, 400, mySel, m_UncNames, ExpUncertaintyName);
   h_Fjet2_M                = new TH1Fs(sampleName+"_Fjet2_M", "", 50, 0, 400, mySel, m_UncNames, ExpUncertaintyName);
   h_FJet1_Eta              = new TH1Fs(sampleName+"_FJet1_Eta", "", 50, -4, 4, mySel, m_UncNames, ExpUncertaintyName);
   h_FJet2_Eta              = new TH1Fs(sampleName+"_FJet2_Eta", "", 50, -4, 4, mySel, m_UncNames, ExpUncertaintyName);
   h_DeltaPhi_FJ            = new TH1Fs(sampleName+"_DeltaPhi_FJ", "", 50, -5, 5, mySel, m_UncNames, ExpUncertaintyName);
   h_NBtags                 = new TH1Fs(sampleName+"_NBtags", "", 50, 0, 10, mySel, m_UncNames, ExpUncertaintyName);
   h_HiggsCandidate_merged  = new TH1Fs(sampleName+"_HiggsCandidate_merged", "", 10, 50, 300, mySel, m_UncNames, ExpUncertaintyName);
   h_WBosonCandidate_merged = new TH1Fs(sampleName+"_WBosonCandidate_merged", "", 10, 50, 250, mySel, m_UncNames, ExpUncertaintyName);
   h_DeltaPhi_lepT_hadchH   = new TH1Fs(sampleName+"_DeltaPhi_lepT_hadchH", "", 50, 0, 5, mySel, m_UncNames, ExpUncertaintyName);
   h_chi2ming1L_lepT        = new TH1Fs(sampleName+"_chi2ming1L_lepT", "", 50, -6, 4, mySel, m_UncNames, ExpUncertaintyName);
   h_lepT_M                 = new TH1Fs(sampleName+"_lepT_M", "", 10, 80, 680, mySel, m_UncNames, ExpUncertaintyName);
   
   h_posLep_chH_M        = new TH1Fs(sampleName+"_posLep_chH_M", "", 50, 300, 1500, mySel, m_UncNames, ExpUncertaintyName);
   h_posLep_chH_Pt       = new TH1Fs(sampleName+"_posLep_chH_Pt", "", 50, 0, 400, mySel, m_UncNames, ExpUncertaintyName);
   h_posLep_chH_Eta      = new TH1Fs(sampleName+"_posLep_chH_Eta", "", 50, -5, 5, mySel, m_UncNames, ExpUncertaintyName);
   h_posLep_higgsJet_Pt  = new TH1Fs(sampleName+"_posLep_higgsJet_Pt", "", 50, 0, 1000, mySel, m_UncNames, ExpUncertaintyName);
   h_posLep_higgsJet_M   = new TH1Fs(sampleName+"_posLep_higgsJet_M", "", 50, 0, 300, mySel, m_UncNames, ExpUncertaintyName);
   h_posLep_higgsJet_Eta = new TH1Fs(sampleName+"_posLep_higgsJet_Eta", "", 50, -5, 5, mySel, m_UncNames, ExpUncertaintyName);
   h_posLep_WBoson_Pt    = new TH1Fs(sampleName+"_posLep_WBoson_Pt", "", 50, 0, 1000, mySel, m_UncNames, ExpUncertaintyName);
   h_posLep_WBoson_Eta   = new TH1Fs(sampleName+"_posLep_WBoson_Eta", "", 50, -5, 5, mySel, m_UncNames, ExpUncertaintyName);
   h_posLep_DeltaPhi_W_h = new TH1Fs(sampleName+"_posLep_DeltaPhi_W_h", "", 50, 0, 5, mySel, m_UncNames, ExpUncertaintyName);
   h_posLep_hadTop_Pt    = new TH1Fs(sampleName+"_posLep_hadTop_Pt", "", 50, 0, 1500, mySel, m_UncNames, ExpUncertaintyName);
   h_posLep_hadTop_Eta   = new TH1Fs(sampleName+"_posLep_hadTop_Eta", "", 50, -5, 5, mySel, m_UncNames, ExpUncertaintyName);
   h_posLep_hadTop_M     = new TH1Fs(sampleName+"_posLep_hadTop_M", "", 50, 0, 1500, mySel, m_UncNames, ExpUncertaintyName);
   
   h_TrackJet_PT   = new TH1Fs(sampleName+"_TrackJet_PT", "", 50, 0, 800, mySel, m_UncNames, ExpUncertaintyName);
   h_TrackJet_Eta  = new TH1Fs(sampleName+"_TrackJet_Eta", "", 50, -5, 5, mySel, m_UncNames, ExpUncertaintyName);
   h_TrackJet_Phi  = new TH1Fs(sampleName+"_TrackJet_Phi", "", 50, -5, 5, mySel, m_UncNames, ExpUncertaintyName);
   h_TRackJet_M    = new TH1Fs(sampleName+"_TRackJet_M", "", 50, 0, 50, mySel, m_UncNames, ExpUncertaintyName);
   h_NBtags_TrackJ = new TH1Fs(sampleName+"_NBtags_TrackJ", "", 50, 0, 5, mySel, m_UncNames, ExpUncertaintyName);
   h_NmatchVR_HJ   = new TH1Fs(sampleName+"_NBtags_TrackJ", "", 50, 0, 5, mySel, m_UncNames, ExpUncertaintyName);
   h_DeltaR_VRJ    = new TH1Fs(sampleName+"_DeltaR_VRJ", "", 50, 0, 5, mySel, m_UncNames, ExpUncertaintyName);

   Chi2LepT_negLepton    = new TH1F("Chi2LepT_posLepton","",50,-1,10);
   Chi2HadH_negLepton    = new TH1F("Chi2HadH_posLepton","",50,-3,5);
   Fjet_minM             = new TH1F("Fjet_minM","", 50, 0, 500);
   Fjet_maxM             = new TH1F("Fjet_maxM","", 50, 0, 500);
   Fjet_M                = new TH1F("Fjet_M","", 50, 0, 500);
   LeadingFJets_DPhi     = new TH1F("LeadingFJets_DPhi","", 50, -4, 4);
   LeadingFJets_M        = new TH1F("LeadingFJets_M","", 50, 0, 1500);
   NBJets                = new TH1F("NBJets","", 50, 0, 5);
   Chi2HadT_negLepton    = new TH1F("Chi2HadT_negLepton","",50,-3,10);
   Mwt                   = new TH1F("Mwt","",50, 0, 400);
   Leptonic_Reco_W_Pt    = new TH1F("Leptonic_Reco_W_Pt","",50, 0, 800);
   Leptonic_Reco_W_M     = new TH1F("Leptonic_Reco_W_M","",50, 0, 300);
   Leptonic_Reco_W_Eta   = new TH1F("Leptonic_Reco_W_Eta","",50, -5, 5);
   Leptonic_Reco_chH_M   = new TH1F("Leptonic_Reco_chH_M","",50, 0, 1500);
   Leptonic_Reco_chH_Pt  = new TH1F("Leptonic_Reco_chH_Pt","",50, 0, 600);
   Leptonic_Reco_chH_Eta = new TH1F("Leptonic_Reco_chH_Eta","",50, -5, 5);
   DPhi_LepW_chH         = new TH1F("DPhi_LepW_chH","",50, -3, 5);
   minDeltaR             = new TH1F("mindR","",50, 0, 10);
   NLargeRJets           = new TH1F("NLargeRJets","", 50, 0, 5);
   overlap_Jets_negLep   = new TH1F("overlap_Jets_negLep","", 50, 0, 5);
   NTrackJets            = new TH1F("NTrackJets","", 50, 0, 15);

   NBtags_TrackJ                = new TH1F("NBtags_TrackJ","", 50, 0, 5);
   DPhi_HW_leptonicDecay        = new TH1F("DPhi_HW_leptonicDecay","", 50, 0, 4);
   DPhi_HW_hadronicDecay        = new TH1F("DPhi_HW_hadronicDecay","", 50, 0, 4);
   M_WH_hadronic                = new TH1F("M_WH_hadronic","", 26, 500, 2000);
   M_WH_leptonic                = new TH1F("M_WH_leptonic","", 26, 500, 2000);
   M_leadingFJet_hadronic       = new TH1F("M_leadingFJet_hadronic","", 50, 0, 400);
   M_secondleadingFJet_hadronic = new TH1F("M_secondleadingFJet_hadronic","", 50, 0, 300);
   M_leadingFJet_leptonic       = new TH1F("M_leadingFJet_leptonic","", 50, 0, 300);

   // Set object pointer
   Description           = 0;
   MET                   = 0;
   Lep1p4                = 0;
   Lep2p4                = 0;           
   VBFjet1_merged        = 0;
   VBFjet2_merged        = 0;
   VBFjet1_resolved      = 0;
   VBFjet2_resolved      = 0;
   FatJet_M              = 0;
   FatJet_PT             = 0;
   FatJet_Eta            = 0;
   FatJet_Phi            = 0;
   TrackJet_M            = 0;
   TrackJet_PT           = 0;
   TrackJet_Eta          = 0;
   TrackJet_Phi          = 0;
   signal_Jet_M          = 0;
   forward_Jet_M         = 0;
   signal_Jet_PT         = 0;
   forward_Jet_PT        = 0;
   signal_Jet_Eta        = 0;
   signal_Jet_Phi        = 0;
   forward_Jet_Eta       = 0;
   forward_Jet_Phi       = 0;
   btag_score_selectJet  = 0;
   btag_score_signalJet  = 0;
   btag_score_forwardJet = 0;
   TrackJet_btagWeight   = 0;
   btag_score_selectJet  = 0;
   btag_score_signalJet  = 0;
   btag_score_forwardJet = 0;
   nbTagsInFJ            = 0;
   nbTrkjetsInFJ         = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);
   
   fChain->SetBranchAddress("nJet", &nJet, &b_nJet);
   fChain->SetBranchAddress("nTags", &nTags, &b_nTags);
   fChain->SetBranchAddress("nFatJets", &nFatJets, &b_nFatJets);
   fChain->SetBranchAddress("nTrkJets", &nTrkJets, &b_nTrkJets);
   fChain->SetBranchAddress("RunNumber", &RunNumber, &b_RunNumber);
   fChain->SetBranchAddress("nBTrkJets", &nBTrkJets, &b_nBTrkJets);
   fChain->SetBranchAddress("nTrackJets", &nTrackJets, &b_nTrackJets);
   fChain->SetBranchAddress("EventNumber", &EventNumber, &b_EventNumber);
   fChain->SetBranchAddress("passedTauVeto", &passedTauVeto, &b_passedTauVeto);
   fChain->SetBranchAddress("passedTrigger", &passedTrigger, &b_passedTrigger);
   fChain->SetBranchAddress("MCChannelNumber", &MCChannelNumber, &b_MCChannelNumber);
   fChain->SetBranchAddress("nbTagsOutsideFJ", &nbTagsOutsideFJ, &b_nbTagsOutsideFJ);
   fChain->SetBranchAddress("lep1charge", &lep1charge, &b_lep1charge);
   fChain->SetBranchAddress("lep2charge", &lep2charge, &b_lep2charge);
   fChain->SetBranchAddress("flavL1", &flavL1, &b_flavL1);
   fChain->SetBranchAddress("flavL2", &flavL2, &b_flavL2);
   fChain->SetBranchAddress("isMediumL1", &isMediumL1, &b_isMediumL1);
   fChain->SetBranchAddress("isMediumL2", &isMediumL2, &b_isMediumL2);
   fChain->SetBranchAddress("mLL", &mLL, &b_mLL);
   fChain->SetBranchAddress("ptL1", &ptL1, &b_ptL1);
   fChain->SetBranchAddress("ptL2", &ptL2, &b_ptL2);
   fChain->SetBranchAddress("etaL1", &etaL1, &b_etaL1);
   fChain->SetBranchAddress("etaL2", &etaL2, &b_etaL2);
   fChain->SetBranchAddress("ActualMu", &ActualMu, &b_ActualMu);
   fChain->SetBranchAddress("EventWeight", &EventWeight, &b_EventWeight);
   fChain->SetBranchAddress("Description", &Description, &b_Description);
   fChain->SetBranchAddress("MET", &MET, &b_MET);
   fChain->SetBranchAddress("Lep1p4", &Lep1p4, &b_Lep1p4);
   fChain->SetBranchAddress("Lep2p4", &Lep2p4, &b_Lep2p4);
   fChain->SetBranchAddress("VBFjet1_merged", &VBFjet1_merged, &b_VBFjet1_merged);
   fChain->SetBranchAddress("VBFjet2_merged", &VBFjet2_merged, &b_VBFjet2_merged);
   fChain->SetBranchAddress("VBFjet1_resolved", &VBFjet1_resolved, &b_VBFjet1_resolved);
   fChain->SetBranchAddress("VBFjet2_resolved", &VBFjet2_resolved, &b_VBFjet2_resolved);
   fChain->SetBranchAddress("FatJet_M", &FatJet_M, &b_FatJet_M);
   fChain->SetBranchAddress("FatJet_PT", &FatJet_PT, &b_FatJet_PT);
   fChain->SetBranchAddress("FatJet_Eta", &FatJet_Eta, &b_FatJet_Eta);
   fChain->SetBranchAddress("FatJet_Phi", &FatJet_Phi, &b_FatJet_Phi);
   fChain->SetBranchAddress("signal_Jet_M", &signal_Jet_M, &b_signal_Jet_M);
   fChain->SetBranchAddress("forward_Jet_M", &forward_Jet_M, &b_forward_Jet_M);
   fChain->SetBranchAddress("signal_Jet_PT", &signal_Jet_PT, &b_signal_Jet_PT);
   fChain->SetBranchAddress("forward_Jet_PT", &forward_Jet_PT, &b_forward_Jet_PT);
   fChain->SetBranchAddress("signal_Jet_Eta", &signal_Jet_Eta, &b_signal_Jet_Eta);
   fChain->SetBranchAddress("signal_Jet_Phi", &signal_Jet_Phi, &b_signal_Jet_Phi);
   fChain->SetBranchAddress("forward_Jet_Eta", &forward_Jet_Eta, &b_forward_Jet_Eta);
   fChain->SetBranchAddress("forward_Jet_Phi", &forward_Jet_Phi, &b_forward_Jet_Phi);
   fChain->SetBranchAddress("btag_score_selectJet", &btag_score_selectJet, &b_btag_score_selectJet);
   fChain->SetBranchAddress("btag_score_signalJet", &btag_score_signalJet, &b_btag_score_signalJet);
   fChain->SetBranchAddress("btag_score_forwardJet", &btag_score_forwardJet, &b_btag_score_forwardJet);
   fChain->SetBranchAddress("TrackJet_PT",&TrackJet_PT, &b_TrackJet_PT);
   fChain->SetBranchAddress("TrackJet_Eta",&TrackJet_Eta, &b_TrackJet_Eta);
   fChain->SetBranchAddress("TrackJet_Phi", &TrackJet_Phi, &b_TrackJet_Phi);
   fChain->SetBranchAddress("TrackJet_M", &TrackJet_M, &b_TrackJet_M);
   fChain->SetBranchAddress("TrackJet_btagWeight", &TrackJet_btagWeight, &b_TrackJet_btagWeight);
   Notify();
}

Bool_t EventLoop_2Lep::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void EventLoop_2Lep::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t EventLoop_2Lep::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef EventLoop_cxx
