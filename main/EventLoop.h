
//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Fri Nov 15 13:27:20 2019 by ROOT version 6.18/00
// from TTree Nominal/Nominal
// found on file: ../my_analysis_dir/run/submitDir_HIGG5D2_1L-MC16a-HVT-32-14-TCC-Wplus-2019-11-15_12h09/data-EasyTree/ttbar_nonallhad_PwPy8.root
//////////////////////////////////////////////////////////

#ifndef EventLoop_h
#define EventLoop_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "string"
#include "TLorentzVector.h"
#include "vector"
#include <iostream>

#include "utilis/Chi2_minimization.h"	
#include "utilis/NeutrinoBuilder.h"
#include "TLorentzVector.h"
#include "TH1F.h"
#include <stdio.h>
#include <chrono>
#include <time.h>
#include "TH1Fs/TH1Fs.h"
#include "TMVA/Reader.h"
#include <TString.h>
#include <map>
//#include "main/xsec.h"

class EventLoop {
public :
   TTree           *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain
   TTree           *m_myTree_hadronic; //!pointer to the output TTree 
   TTree           *m_myTree_Leptonic; //!pointer to the output TTree
   TTree           *m_myTree_Event; //!pointer to the output TTree
   TMVA::Reader    *m_reader_qqbb;  //!
   TMVA::Reader    *m_reader_lvbb;  //!
   TTree           *Nom;

   // Fixed size dimensions of array or collections stored in the TTree if any.
   
   // Declaration of leaf types
   Int_t           nJet;
   Int_t           nFatJets;
   Int_t           mcChannelNumber;
   Int_t           TopHeavyFlavorFilterFlag;
   Int_t           nMuons;
   Int_t           nElectrons;
   Long_t          eventNumber;
   Int_t           nPrimaryVtx;
   Int_t           RunNumber;
   Int_t           randomRunNumber;
   Int_t           isTriggered;
   Int_t           nTaus;
   Int_t           HF_SimpleClassification;
   Int_t           jet_truthflav;   //only for MC
   Int_t           boosted_ljets_ejets_2015_DL1r;
   Int_t           boosted_ljets_ejets_2016_DL1r;
   Int_t           boosted_ljets_ejets_2017_DL1r;
   Int_t           boosted_ljets_ejets_2018_DL1r;
   Int_t           boosted_ljets_mujets_2015_DL1r;
   Int_t           boosted_ljets_mujets_2016_DL1r;
   Int_t           boosted_ljets_mujets_2017_DL1r;
   Int_t           boosted_ljets_mujets_2018_DL1r;
   Int_t           boosted_ljets_mujets_2015_DL1r_HighPtMuon;
   Int_t           boosted_ljets_mujets_2016_DL1r_HighPtMuon;
   Int_t           boosted_ljets_mujets_2017_DL1r_HighPtMuon;
   Int_t           boosted_ljets_mujets_2018_DL1r_HighPtMuon;


   ////Int_t           hasBadMuon;
   bool            HLT_mu26_ivarmedium;
   bool            HLT_mu50;
   bool            HLT_e60_lhmedium_nod0;
   bool            HLT_e140_lhloose_nod0;
   bool            HLT_e26_lhtight_nod0_ivarloose;
   Float_t         Mu;
   ////Float_t         ActualMu;
   Float_t         EventWeight;
   Float_t         GenFiltHT;

   //The weights given below are only for MC
   
   Float_t         weight_normalise;
   Float_t         weight_pileup;
   Float_t         weight_mc;
   Float_t         weight_leptonSF;
   Float_t         weight_bTagSF_DL1r_Continuous;
   Float_t         weight_jvt; 
   
   
   Float_t         met;
   Float_t         met_phi;
   Float_t         HT_all;
   //std::vector<float>   *mc_generator_weights;
   std::vector<float>   *el_pt;
   std::vector<float>   *el_eta;
   std::vector<float>   *el_phi;
   std::vector<float>   *el_e;
   std::vector<float>   *el_charge;
   std::vector<float>   *mu_pt;
   std::vector<float>   *mu_eta;
   std::vector<float>   *mu_phi;
   std::vector<float>   *mu_e;
   std::vector<float>   *mu_charge;
   std::vector<float>   *FatJet_M;
   std::vector<float>   *FatJet_PT;
   std::vector<float>   *FatJet_Eta;
   std::vector<float>   *FatJet_Phi;
   std::vector<float>   *FatJet_Xbb2020v3_Higgs;
   std::vector<float>   *FatJet_Xbb2020v3_QCD;
   std::vector<float>   *FatJet_Xbb2020v3_Top;
   std::vector<float>   *signal_Jet_E;
   std::vector<float>   *signal_Jet_PT;
   std::vector<float>   *signal_Jet_Eta;
   std::vector<float>   *signal_Jet_Phi;
   std::vector<int>   *signal_Jet_truthflav;  //only for MC
   std::vector<int>     *signal_Jet_tagWeightBin_DL1r_Continuous;
   
   // Given below variables only for MC
   std::vector<int>     *truth_pdgid;
   std::vector<int>     *truth_status;
   std::vector<float>   *truth_pt; 
   std::vector<float>   *truth_eta; 
   std::vector<float>   *truth_phi; 
   std::vector<float>   *truth_m;
   std::vector<long>   *truth_tthbb_info; 

   std::vector<TLorentzVector*> NeutrinoVec;
   map<TString, bool> pass_sel;

   // List of branches
   TBranch        *b_nJet;   //!
   TBranch        *b_nFatJets;   //!
   TBranch        *b_RunNumber;   //!
   TBranch        *b_randomRunNumber;   //!
   TBranch        *b_eventNumber;   //!
   TBranch        *b_nMuons;   //!
   TBranch        *b_nElectrons;   //!
   TBranch        *b_nPrimaryVtx;   //!
   TBranch        *b_nTaus;   //!
   TBranch        *b_HF_SimpleClassification; //!
   TBranch        *b_mcChannelNumber;
   TBranch        *b_GenFiltHT;
   TBranch        *b_TopHeavyFlavorFilterFlag;
   ////TBranch        *b_hasBadMuon;   //!
   TBranch        *b_HLT_mu26_ivarmedium;   //!
   TBranch        *b_HLT_mu50;   //!
   TBranch        *b_HLT_e60_lhmedium_nod0;   //!
   TBranch        *b_HLT_e140_lhloose_nod0;   //!
   TBranch        *b_HLT_e26_lhtight_nod0_ivarloose;   //!
   TBranch        *b_Mu;   //!
   ////TBranch        *b_ActualMu;   //!
   ////TBranch        *b_weight_normalise;   //!
   
   //The weights given below are only for MC
   
   TBranch        *b_weight_pileup;   //!
   TBranch        *b_weight_normalise;   //!
   TBranch        *b_weight_mc;   //!
   TBranch        *b_weight_leptonSF;   //!
   TBranch        *b_weight_bTagSF_DL1r_Continuous;   //!
   TBranch        *b_weight_jvt;   //! 

   TBranch        *b_MET;   //!
   TBranch        *b_MET_phi;   //!
   TBranch        *b_HT_all;

   //TBranch        *b_mc_generator_weights;   //!  
   TBranch        *b_el_pt;   //!
   TBranch        *b_el_eta;   //!
   TBranch        *b_el_phi;   //!
   TBranch        *b_el_e;   //!
  // TBranch        *b_el_cl_eta;   //!
   TBranch        *b_el_charge;   //!
   TBranch        *b_mu_pt;   //!
   TBranch        *b_mu_eta;   //!
   TBranch        *b_mu_phi;   //!
   TBranch        *b_mu_e;   //!
   TBranch        *b_mu_charge;   //!
   TBranch        *b_FatJet_M;   //!
   TBranch        *b_FatJet_PT;   //!
   TBranch        *b_FatJet_Eta;   //!
   TBranch        *b_FatJet_Phi;   //!
   TBranch        *b_FatJet_Xbb2020v3_Higgs;
   TBranch        *b_FatJet_Xbb2020v3_QCD;
   TBranch        *b_FatJet_Xbb2020v3_Top;
   TBranch        *b_signal_Jet_E;   //!
   TBranch        *b_signal_Jet_PT;   //!
   TBranch        *b_signal_Jet_Eta;   //!
   TBranch        *b_signal_Jet_Phi;   //!
   TBranch        *b_signal_Jet_truthflav;   //!   //only for MC
   TBranch        *b_signal_Jet_tagWeightBin_DL1r_Continuous;   //!
   
   //only for MC
   
   TBranch        *b_truth_pt;   //!
   TBranch        *b_truth_eta;   //!
   TBranch        *b_truth_phi;   //!
   TBranch        *b_truth_m;   //!
   TBranch        *b_truth_pdgid;   //! 
   TBranch        *b_truth_status;   //!
   TBranch        *b_truth_tthbb_info; //!  

   TBranch        *b_boosted_ljets_ejets_2015_DL1r;
   TBranch        *b_boosted_ljets_ejets_2016_DL1r;
   TBranch        *b_boosted_ljets_ejets_2017_DL1r;
   TBranch        *b_boosted_ljets_ejets_2018_DL1r;
   TBranch        *b_boosted_ljets_mujets_2015_DL1r;
   TBranch        *b_boosted_ljets_mujets_2016_DL1r;
   TBranch        *b_boosted_ljets_mujets_2017_DL1r;
   TBranch        *b_boosted_ljets_mujets_2018_DL1r;
   TBranch        *b_boosted_ljets_mujets_2015_DL1r_HighPtMuon;
   TBranch        *b_boosted_ljets_mujets_2016_DL1r_HighPtMuon;
   TBranch        *b_boosted_ljets_mujets_2017_DL1r_HighPtMuon;
   TBranch        *b_boosted_ljets_mujets_2018_DL1r_HighPtMuon;

   EventLoop(TTree *tree=0, TString sampleName="", TString MCName="", TString MCDataPeriode="", TString ExpUncertaintyName="Nominal", TString WP="", TString TOPCON="");
   void Write(TDirectory *dir, std::string dirname);
   void WriteTreesToFile(TFile *outFile);
   void	Sort_Jets(std::vector<TLorentzVector> *Jets);
   void	Sort_Jets_SJ(std::vector<TLorentzVector> *Jets, std::vector<int> *is_tagged);
   void Set_Jet_observables();
   void SetTruthParticles_tthbb();
   //different SetTruthParticles variant for BDT training
   double FindXbbTagged_Jets();
   double FindXbbTagged_Jets_v2();
   void SetJetVectors();
   void SetLeptonVectors();
   double GetMwt();
   
   bool PassEventSelectionResolved();
   bool PassEventSelectionBoosted();
   int GetBTagCategory_PreSel();
   int GetTagWeightBin(double btag_score);
   TLorentzVector GetWBoson(bool &status);
   
   std::vector<TLorentzVector*> GetNeutrinos(TLorentzVector* L, TLorentzVector* MET);
   virtual ~EventLoop();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree, TString sampleName, TString ExpUncertaintyName);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);

    
   Chi2_minimization* myMinimizer = new Chi2_minimization("MeV");
   NeutrinoBuilder* m_NeutrinoBuilder;
   
   TH1Fs *h_evw;
   TH1Fs *h_LJMass;
   TH1Fs *h_LJPt;
   TH1Fs *h_LJEta;
   TH1Fs *h_SLJMass;
   TH1Fs *h_SLJPt;
   TH1Fs *h_SLJEta;

   TH1Fs *h_HT;
   TH1Fs *h_HT_all;

   TH1Fs *h_HT_5j;
   TH1Fs *h_HT_all_5j;
   TH1Fs *h_HT_6j;
   TH1Fs *h_HT_all_6j;
   TH1Fs *h_HT_7j;
   TH1Fs *h_HT_all_7j;
   TH1Fs *h_HT_8j;
   TH1Fs *h_HT_all_8j;
   TH1Fs *h_HT_9j;
   TH1Fs *h_HT_all_9j;

   TH1Fs *h_HT_1b;
   TH1Fs *h_HT_all_1b;

   TH1Fs *h_HT_5j_1b;
   TH1Fs *h_HT_all_5j_1b;
   TH1Fs *h_HT_6j_1b;
   TH1Fs *h_HT_all_6j_1b;
   TH1Fs *h_HT_7j_1b;
   TH1Fs *h_HT_all_7j_1b;
   TH1Fs *h_HT_8j_1b;
   TH1Fs *h_HT_all_8j_1b;
   TH1Fs *h_HT_9j_1b;
   TH1Fs *h_HT_all_9j_1b;

   TH1Fs *h_HT_1c;
   TH1Fs *h_HT_all_1c;

   TH1Fs *h_HT_5j_1c;
   TH1Fs *h_HT_all_5j_1c;
   TH1Fs *h_HT_6j_1c;
   TH1Fs *h_HT_all_6j_1c;
   TH1Fs *h_HT_7j_1c;
   TH1Fs *h_HT_all_7j_1c;
   TH1Fs *h_HT_8j_1c;
   TH1Fs *h_HT_all_8j_1c;
   TH1Fs *h_HT_9j_1c;
   TH1Fs *h_HT_all_9j_1c;

   TH1Fs *h_HT_1l;
   TH1Fs *h_HT_all_1l;

   TH1Fs *h_HT_5j_1l;
   TH1Fs *h_HT_all_5j_1l;
   TH1Fs *h_HT_6j_1l;
   TH1Fs *h_HT_all_6j_1l;
   TH1Fs *h_HT_7j_1l;
   TH1Fs *h_HT_all_7j_1l;
   TH1Fs *h_HT_8j_1l;
   TH1Fs *h_HT_all_8j_1l;
   TH1Fs *h_HT_9j_1l;
   TH1Fs *h_HT_all_9j_1l;

   TH1Fs *h_LJMass_1b;
   TH1Fs *h_LJPt_1b;
   TH1Fs *h_LJEta_1b;
   TH1Fs *h_SLJMass_1b;
   TH1Fs *h_SLJPt_1b;
   TH1Fs *h_SLJEta_1b;

   TH1Fs *h_LJMass_1c;
   TH1Fs *h_LJPt_1c;
   TH1Fs *h_LJEta_1c;
   TH1Fs *h_SLJMass_1c;
   TH1Fs *h_SLJPt_1c;
   TH1Fs *h_SLJEta_1c;

   TH1Fs *h_LJMass_1l;
   TH1Fs *h_LJPt_1l;
   TH1Fs *h_LJEta_1l;
   TH1Fs *h_SLJMass_1l;
   TH1Fs *h_SLJPt_1l;
   TH1Fs *h_SLJEta_1l;

   //reweighted histogram definition

   vector<double> m_EventWeights;
   vector<double>  m_ones;
   vector<TString> m_UncNames;
   vector<float> m_LepCh;
   
   vector<TString> mySel;

   std::vector<TLorentzVector> bQuarks;
   std::vector<TLorentzVector> LightQuarks;   
   std::vector<TLorentzVector> Leptons;
   std::vector<TLorentzVector> Muons;
   std::vector<TLorentzVector> Electrons;
   std::vector<TLorentzVector> GenLevLeptons;
   std::vector<TLorentzVector> GenLevLeptons_I;
   std::vector<TLorentzVector> GenLevLeptons_II;
   std::vector<TLorentzVector> Jets;
   std::vector<TLorentzVector> FJets;
   std::vector<TLorentzVector> bTaggedJets;
   std::vector<TLorentzVector> TrkJets;
   std::vector<TLorentzVector> TruthbJets;
   std::vector<TLorentzVector> TruthlightJets;
   std::vector<TLorentzVector> TruthbJets_MatchPar;
   std::vector<TLorentzVector> TruthlightJets_MatchPar;
   std::vector<int> Jets_PCbtag;
   //std::vector<int> TrkJets_PCbtag;
   //std::vector<int> nTaggedVRTrkJetsInFJet;

   TLorentzVector Lepton4vector;
   TLorentzVector Had_top;
   TLorentzVector MET;
   TLorentzVector Higgs;
   TLorentzVector Wplus;
   TLorentzVector Higgs_tr;
   TLorentzVector Wplus_tr;
   TLorentzVector Higgs_pole;
   TLorentzVector Wplus_had_pole;
   TLorentzVector Wplus_lep_pole;
   TLorentzVector Wminus;
   TLorentzVector Higgs_LV;
   TLorentzVector Wplus_LV;
   TLorentzVector Higgs_p1;
   TLorentzVector Higgs_p2;
   TLorentzVector Wplus_p1;
   TLorentzVector Wplus_p2;
   TLorentzVector LepTop;
   TLorentzVector HadTop;
   bool   found_Wplus_had;
   bool   found_Wplus_lep;
   bool   found_Higgs;
   bool   found_Higgs_1;
   bool   found_Higgs_2;
   bool   found_LepTop;
   bool   found_HadTop;
   bool   m_countdRj1j2;
   bool   m_countdRj3j4;
   int    m_is_Signal;
   float  m_costhetastar;
   int    m_eventnum;
   int    EventNum;
   float  m_DXbb;
   int    count_nFJ;
   int    count_nFJ_v2;
   int    FJ_size;
   TString m_evntype; 
   TLorentzVector sel_jet;

   int    m_NTags_S;
   int    m_NTags;

   int    m_NTags_PreSel;
   double m_LepTopMass;
   int    m_NTags_caloJ;
   int    m_NTags_trkJ;
   int    m_NTags_Higgs;
   int    m_ntagsOutside;
   int    m_bTagCategory;
   int    m_btagCategoryBin;
   int    m_index_H1;
   int    m_index_H2;
   int    m_index_W1;
   int    m_index_W2;
   int    m_index_bj;
   int    m_true_count;
   int    m_true_count_lvbb;
   int    m_accu_count;
   int    m_accu_count2;
   int    m_accu_count4;
   int    m_accu_count5;
   int    m_accu_count6;
   float m_min_dRTruth;
   float m_min_chi2;
   float m_min_DeltaPhiJETMET;
   float m_MaxMVA_Response;
   float m_HT;
   float m_HT_all;
   float m_HT_bjets;
   float m_maxEta_bjets;
   float m_maxPT_bjets;
   float m_mWT;
   float m_Lep_pT;
   float m_Lep_Eta;
   float m_mVH;
   float m_DeltaPhi_HW;
   float m_DeltaEta_HW;
   float m_Wleptonic_pT;
   float m_Wleptonic_Eta;
   float m_MassTruth;
   float m_MET;
   float m_HT6j;
   float m_dR_lj_min;
   float m_dR_lj_max;
   float m_bTagScoreSum4j;
   float m_bTagScoreSum6j;
   float m_H_mass;    
   float m_H_pT;
   float m_pTjH1;
   float m_pTjH2;
   float m_btagjH1;
   float m_btagjH2;
   float m_dRjjH;
   float m_Wp_mass;
   float m_Wp_pT;
   float m_pTjWp1;
   float m_pTjWp2;
   float m_btagjWp1;
   float m_btagjWp2;
   float m_dRjjWp;
   float m_Phi_HW;
   float m_Eta_HW;
   float m_dR_HW;
   float m_mass_VH;
   float m_pTH_over_mvH;

   float m_maxMVA;
   float m_pTHomVH;
   float m_pTWomVH;
   float m_btagH1;
   float m_btagH2;
   float m_btagW1;
   float m_btagW2;
   float m_H;
   float m_W;

   float m_ptW_over_mvH;
   float m_weight_scale;
   int m_flag_tt;
   float m_weight_norm;
   float m_norm;
   float xsec;
   float kfac;
   float sum_weights;
   float m_scale;
   int m_topcon;

   float massWH;
   int  nJets;
   float maxMVA;
   float pTWplus;
   float pTHiggs;

   float m_min_dRq1j1;
   float m_min_dRq2j2;
   float m_min_dRb1j1;
   float m_min_dRb2j2;

   float m_min_dRHjj;
   float m_min_dRWjj;

   float pTlep;
   float etalep;
   float DelEtaHW;
   float DelPhiHW;
   float HT_bjet;
   float pTHiggs_over_massWH;
   float pTWplus_over_massWH;
   float massHiggs;
   float massWplus;
   float btagj1H;
   float btagj2H; 
   float btagj1W;
   float btagj2W;
   float leptop_mass;
   float top_mass;
   float pTj1H;
   float pTj2H;
   float pTj1W;
   float pTj2W;
   float etaj1H;
   float etaj2H;
   float etaj1W;
   float etaj2W;

   int nBTags;
   int HFClass;
   int mcChanNbr;
   int TopHeavyFF;
   float weight_btagSF;
   float weight_normal;
   float weight_MC;
   float weight_jetvt;
   float weight_lepSF;
   float Mwt;
   float HT;
   int ttFlag;
   int ctrl;
   int ctrl_v2;
   
   //muon weight 


  
   float weight_pu;
   float weight_pu_UP;
   float weight_pu_DOWN;
   float weight_jetvt_UP;
   float weight_jetvt_DOWN;
   vector<float> weight_btagSF_CUP;
   vector<float> weight_btagSF_CDOWN;
   vector<float> weight_btagSF_BUP;
   vector<float> weight_btagSF_BDOWN;
   vector<float> weight_btagSF_LUP;
   vector<float> weight_btagSF_LDOWN;
   float weight_normal_mu2f2;
   float weight_normal_mu5f5;
   float weight_normal_fsrUP;
   float weight_normal_fsrDOWN;
   float weight_normal_varUP;
   float weight_normal_varDOWN;
   float weight_mu2f2;
   float weight_mu5f5;
   float weight_fsrUP;
   float weight_fsrDOWN;
   float weight_varUP;
   float weight_varDOWN;
   //bool JetPairqqbb;
   vector<double>  EvtWeight_wott;
   vector<double>  EvtWeight_wtt;
};

#endif

#ifdef EventLoop_cxx
EventLoop::EventLoop(TTree *tree, TString sampleName, TString MCName, TString MCDataPeriode, TString ExpUncertaintyName, TString WP, TString TOPCON) : fChain(0) {

// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
	std::cerr << "Error in EventLoop::EventLoop(): tree is nullptr" << std::endl;
	return;
   }

   if(WP == "85p"){
      m_btagCategoryBin        = 2;
   }
   if(WP == "77p"){
      m_btagCategoryBin        = 3;
   } 
   if(WP == "70p"){
      m_btagCategoryBin        = 4;
   }
   if(WP == "60p"){
      m_btagCategoryBin        = 5; 
   }
   //std::cout<<"Passed loop"<<std::endl;

   
   if(sampleName=="410470"||sampleName=="411073"||sampleName=="411074"||sampleName=="411075"||sampleName=="411076"||sampleName=="411077"||sampleName=="411078"||sampleName=="407342"||sampleName=="407343"||sampleName=="407344")
   {
     m_flag_tt = 1;
   }
   else
   {
    m_flag_tt = 0;
   }
   
   /*
   if(sampleName=="tt_PP8.root" || sampleName=="tt_PP8filtered.root")
   {
     m_flag_tt = 1;
   }
   else
   {
    m_flag_tt = 0;
   }
   */
   
   /*if(sampleName.Contains("tt_PP8.root") && MCDataPeriode == "MC16e")
   {
     m_scale = 0.5;
   }
   else
   {
    m_scale = 1;
   }*/


   if(TOPCON == "250"){
      m_topcon = 250;
   }
   if(TOPCON == "300"){
      m_topcon = 300;
   } 
   if(TOPCON == "350"){
      m_topcon = 350;
   }
   if(TOPCON == "200"){
      m_topcon = 200;
   }
   if(TOPCON == "225"){
      m_topcon = 225;
   } 
   if(TOPCON == "275"){
      m_topcon = 275;
   }
   if(TOPCON == "325"){
      m_topcon = 325;
   }

   if(TOPCON == "4"){
      m_topcon = 4;
   }
   if(TOPCON == "5"){
      m_topcon = 5;
   } 
   if(TOPCON == "6"){
      m_topcon = 6;
   }
   if(TOPCON == "7"){
      m_topcon = 7;
   }
   if(TOPCON == "8"){
      m_topcon = 8;
   } 
   if(TOPCON == "9"){
      m_topcon = 9;
   }
   if(TOPCON == "10"){
      m_topcon = 10;
   }
   if(TOPCON == "11"){
      m_topcon = 11;
   }
   if(TOPCON == "12"){
      m_topcon = 12;
   } 

   /*
   if(MCDataPeriode == "MC16a")
   {
     m_norm = (dsid_xsec[int(sampleName)]*dsid_kfac[int(sampleName)])/(sum_weights);
   } 

   if(MCDataPeriode == "MC16d")
   {
      m_norm = (dsid_xsec[int(sampleName)]*dsid_kfac[int(sampleName)])/(sum_weights);
   }
   if(MCDataPeriode == "MC16e")
   {
      m_norm = (dsid_xsec[int(sampleName)]*dsid_kfac[int(sampleName)])/(sum_weights);
   }
   */
   
   
   
   if(sampleName=="410470")
   {  
      xsec= 396.87;
      kfac= 1.13976;
      if(MCDataPeriode == "MC16a")
      {
         sum_weights= 8.72058e+10;
      }

      if(MCDataPeriode == "MC16d")
      {
         sum_weights= 1.09027e+11;
      }

      if(MCDataPeriode == "MC16e")
      {
         sum_weights= 1.45428e+11;
      }
   }

   if(sampleName=="411073")
   {  
      xsec= 2.939;
      kfac= 1.1398;
      if(MCDataPeriode == "MC16a")
      {
         sum_weights= 7.11822e+09;
      }

      if(MCDataPeriode == "MC16d")
      {
         sum_weights= 8.95601e+09;
      }

      if(MCDataPeriode == "MC16e")
      {
         sum_weights= 1.13872e+10;
      }
   }

   if(sampleName=="411074")
   {  
      xsec= 15.50;
      kfac= 1.1398;
      if(MCDataPeriode == "MC16a")
      {
         sum_weights= 7.28905e+09;
      }

      if(MCDataPeriode == "MC16d")
      {
         sum_weights= 9.11059e+09;
      }

      if(MCDataPeriode == "MC16e")
      {
         sum_weights= 1.19536e+10;
      }
   }

   if(sampleName=="411075")
   {  
      xsec= 13.19;
      kfac= 1.1398;
      if(MCDataPeriode == "MC16a")
      {
         sum_weights= 7.27737e+09;
      }

      if(MCDataPeriode == "MC16d")
      {
         sum_weights= 9.09343e+09;
      }

      if(MCDataPeriode == "MC16e")
      {
         sum_weights= 1.19236e+10;
      }
   }

   if(sampleName=="411076")
   {  
      xsec= 0.6740;
      kfac= 1.1397;
      if(MCDataPeriode == "MC16a")
      {
         sum_weights= 3.21333e+09;
      }

      if(MCDataPeriode == "MC16d")
      {
         sum_weights= 4.21891e+09;
      }

      if(MCDataPeriode == "MC16e")
      {
         sum_weights= 5.26083e+09;
      }
   }

   if(sampleName=="411077")
   {  
      xsec= 3.582;
      kfac= 1.1398;
      if(MCDataPeriode == "MC16a")
      {
         sum_weights= 3.61088e+09;
      }

      if(MCDataPeriode == "MC16d")
      {
         sum_weights= 4.49595e+09;
      }

      if(MCDataPeriode == "MC16e")
      {
         sum_weights= 5.95493e+09;
      }
   }

   if(sampleName=="411078")
   {  
      xsec= 3.034;
      kfac= 1.1397;
      if(MCDataPeriode == "MC16a")
      {
         sum_weights= 3.61598e+09;
      }

      if(MCDataPeriode == "MC16d")
      {
         sum_weights= 4.496e+09;
      }

      if(MCDataPeriode == "MC16e")
      {
         sum_weights= 5.9419e+09;
      }
   }
   
   if(sampleName=="407342")
   {  
      xsec= 0.4472249491;
      kfac= 1.1398;
      if(MCDataPeriode == "MC16a")
      {
         sum_weights= 1.43612e+09;
      }

      if(MCDataPeriode == "MC16d")
      {
         sum_weights= 3.36518e+09;
      }

      if(MCDataPeriode == "MC16e")
      {
         sum_weights= 7.85098e+09;
      }
   }

   if(sampleName=="407343")
   {  
      xsec= 2.595354028;
      kfac= 1.1398;
      if(MCDataPeriode == "MC16a")
      {
         sum_weights= 4.60485e+10;
      }

      if(MCDataPeriode == "MC16d")
      {
         sum_weights= 7.78709e+10;
      }

      if(MCDataPeriode == "MC16e")
      {
         sum_weights= 1.80508e+10;
      }
   }

   if(sampleName=="407344")
   {  
      xsec= 18.71057303;
      kfac= 1.1398;
      if(MCDataPeriode == "MC16a")
      {
         sum_weights= 7.24638e+09;
      }

      if(MCDataPeriode == "MC16d")
      {
         sum_weights= 1.78161e+10;
      }

      if(MCDataPeriode == "MC16e")
      {
         sum_weights= 2.37826e+10;
      }
   }

   if(sampleName=="346343")
   {  
      xsec= 0.23082;
      kfac= 1.0;
      if(MCDataPeriode == "MC16a")
      {
         sum_weights= 1.19073e+06;
      }

      if(MCDataPeriode == "MC16d")
      {
         sum_weights= 1.55075e+06;
      }

      if(MCDataPeriode == "MC16e")
      {
         sum_weights= 1.97545e+06;
      }
   }

   if(sampleName=="346344")
   {  
      xsec= 0.22276;
      kfac= 1.0;
      if(MCDataPeriode == "MC16a")
      {
         sum_weights= 2.62455e+06;
      }

      if(MCDataPeriode == "MC16d")
      {
         sum_weights= 3.41184e+06;
      }

      if(MCDataPeriode == "MC16e")
      {
         sum_weights= 4.35099e+06;
      }
   }

   if(sampleName=="346345")
   {  
      xsec= 0.05343;
      kfac= 1.0;
      if(MCDataPeriode == "MC16a")
      {
         sum_weights= 273510;
      }

      if(MCDataPeriode == "MC16d")
      {
         sum_weights= 355568;
      }

      if(MCDataPeriode == "MC16e")
      {
         sum_weights= 454012;
      }
   }

   if(sampleName=="410408")
   {  
      xsec= 0.016046;
      kfac= 1.0;
      if(MCDataPeriode == "MC16a")
      {
         sum_weights= 1611.9;
      }

      if(MCDataPeriode == "MC16d")
      {
         sum_weights= 1918.38 ;
      }

      if(MCDataPeriode == "MC16e")
      {
         sum_weights= 2546.11;
      }
   }

   if(sampleName=="346676")
   {  
      xsec= 0.060140;
      kfac= 1.0;
      if(MCDataPeriode == "MC16a")
      {
         sum_weights= 71816.9;
      }

      if(MCDataPeriode == "MC16d")
      {
         sum_weights= 90501.8 ;
      }

      if(MCDataPeriode == "MC16e")
      {
         sum_weights= 120026;
      }
   }

   if(sampleName=="346678")
   {  
      xsec= 0.016719;
      kfac= 1.0;
      if(MCDataPeriode == "MC16a")
      {
         sum_weights= 15027.4;
      }

      if(MCDataPeriode == "MC16d")
      {
         sum_weights= 18884.5;
      }

      if(MCDataPeriode == "MC16e")
      {
         sum_weights= 25106.3;
      }
   }

   if(sampleName=="410560")
   {  
      xsec= 0.24041;
      kfac= 1.0;
      if(MCDataPeriode == "MC16a")
      {
         sum_weights= 43.7437;
      }

      if(MCDataPeriode == "MC16d")
      {
         sum_weights= 43.7344 ;
      }

      if(MCDataPeriode == "MC16e")
      {
         sum_weights= 57.8341;
      }
   }

   if(sampleName=="410644")
   {  
      xsec= 2.0268;
      kfac= 1.015;
      if(MCDataPeriode == "MC16a")
      {
         sum_weights= 4.05637e+06;
      }

      if(MCDataPeriode == "MC16d")
      {
         sum_weights= 5.0656e+06;
      }

      if(MCDataPeriode == "MC16e")
      {
         sum_weights= 6.69946e+06;
      }
   }

   if(sampleName=="410645")
   {  
      xsec= 1.2676;
      kfac= 1.015;
      if(MCDataPeriode == "MC16a")
      {
         sum_weights= 2.53797e+06;
      }

      if(MCDataPeriode == "MC16d")
      {
         sum_weights= 3.17182e+06;
      }

      if(MCDataPeriode == "MC16e")
      {
         sum_weights= 4.20759e+06;
      }
   }

   if(sampleName=="410646")
   {  
      xsec= 37.935;
      kfac= 0.9450;
      if(MCDataPeriode == "MC16a")
      {
         sum_weights= 3.79094e+08;
      }

      if(MCDataPeriode == "MC16d")
      {
         sum_weights= 4.73547e+08 ;
      }

      if(MCDataPeriode == "MC16e")
      {
         sum_weights= 6.29079e+08;
      }
   }

   if(sampleName=="410647")
   {  
      xsec= 37.905;
      kfac= 0.9457;
      if(MCDataPeriode == "MC16a")
      {
         sum_weights= 3.79068e+08;
      }

      if(MCDataPeriode == "MC16d")
      {
         sum_weights= 4.7245e+08;
      }

      if(MCDataPeriode == "MC16e")
      {
         sum_weights= 6.27923e+08;
      }
   }

   if(sampleName=="410658")
   {  
      xsec= 36.993;
      kfac= 1.191;
      if(MCDataPeriode == "MC16a")
      {
         sum_weights= 9.16561e+08;
      }

      if(MCDataPeriode == "MC16d")
      {
         sum_weights= 1.15403e+09 ;
      }

      if(MCDataPeriode == "MC16e")
      {
         sum_weights= 1.54136e+09;
      }
   }

   if(sampleName=="410659")
   {  
      xsec= 22.175;
      kfac= 1.183;
      if(MCDataPeriode == "MC16a")
      {
         sum_weights= 5.48748e+08;
      }

      if(MCDataPeriode == "MC16d")
      {
         sum_weights= 6.86025e+08;
      }

      if(MCDataPeriode == "MC16e")
      {
         sum_weights= 9.19187e+08;
      }
   }

   if(sampleName=="413023")
   {  
      xsec= 0.13058;
      kfac= 0.94731;
      if(MCDataPeriode == "MC16a")
      {
         sum_weights= 3.15168e+06;
      }

      if(MCDataPeriode == "MC16d")
      {
         sum_weights= 3.96243e+06;
      }

      if(MCDataPeriode == "MC16e")
      {
         sum_weights= 5.29269e+06;
      }
   }
   
   if(sampleName=="412043")
   {  
      xsec= 0.010624;
      kfac= 1.1267;
      if(MCDataPeriode == "MC16a")
      {
         sum_weights= 42612.7;
      }

      if(MCDataPeriode == "MC16d")
      {
         sum_weights= 53248.7;
      }

      if(MCDataPeriode == "MC16e")
      {
         sum_weights= 70784.9;
      }
   }

   if(sampleName=="364250")
   {  
      xsec= 1.2515;
      kfac= 1.0;
      if(MCDataPeriode == "MC16a")
      {
         sum_weights= 7.51881e+06;
      }

      if(MCDataPeriode == "MC16d")
      {
         sum_weights= 1.51851e+07;
      }

      if(MCDataPeriode == "MC16e")
      {
         sum_weights= 1.08084e+07;
      }
   }

   if(sampleName=="364253")
   {  
      xsec= 4.5723;
      kfac= 1.0;
      if(MCDataPeriode == "MC16a")
      {
         sum_weights= 5.35645e+06;
      }

      if(MCDataPeriode == "MC16d")
      {
         sum_weights= 1.10668e+07;
      }

      if(MCDataPeriode == "MC16e")
      {
         sum_weights= 9.2335e+06;
      }
   }

   if(sampleName=="364254")
   {  
      xsec= 12.501;
      kfac= 1.0;
      if(MCDataPeriode == "MC16a")
      {
         sum_weights= 5.1148e+06;
      }

      if(MCDataPeriode == "MC16d")
      {
         sum_weights= 1.0238e+07;
      }

      if(MCDataPeriode == "MC16e")
      {
         sum_weights= 8.48634e+06;
      }
   }

   if(sampleName=="364255")
   {  
      xsec= 3.2348;
      kfac= 1.0;
      if(MCDataPeriode == "MC16a")
      {
         sum_weights= 1.75046e+06;
      }

      if(MCDataPeriode == "MC16d")
      {
         sum_weights= 3.50595e+06;
      }

      if(MCDataPeriode == "MC16e")
      {
         sum_weights= 2.90987e+06;
      }
   }

   if(sampleName=="364288")
   {  
      xsec= 1.4496;
      kfac= 1.0;
      if(MCDataPeriode == "MC16a")
      {
         sum_weights= 203874;
      }

      if(MCDataPeriode == "MC16d")
      {
         sum_weights= 247974;
      }

      if(MCDataPeriode == "MC16e")
      {
         sum_weights= 330362;
      }
   }

   if(sampleName=="364289")
   {  
      xsec= 2.9599;
      kfac= 1.0;
      if(MCDataPeriode == "MC16a")
      {
         sum_weights= 2.24604e+06;
      }

      if(MCDataPeriode == "MC16d")
      {
         sum_weights= 2.77882e+06;
      }

      if(MCDataPeriode == "MC16e")
      {
         sum_weights= 4.31831e+06;
      }
   }

   if(sampleName=="364290")
   {  
      xsec= 0.17154;
      kfac= 1.0;
      if(MCDataPeriode == "MC16a")
      {
         sum_weights= 29840.3;
      }

      if(MCDataPeriode == "MC16d")
      {
         sum_weights= 40270.6;
      }

      if(MCDataPeriode == "MC16e")
      {
         sum_weights= 48496.1;
      }
   }

   if(sampleName=="363355")
   {  
      xsec= 4.35754683;
      kfac= 1.0;
      if(MCDataPeriode == "MC16a")
      {
         sum_weights= 3.49661e+06;
      }

      if(MCDataPeriode == "MC16d")
      {
         sum_weights= 3.49036e+06;
      }

      if(MCDataPeriode == "MC16e")
      {
         sum_weights= 5.79508e+06;
      }
   }

   if(sampleName=="363356")
   {  
      xsec= 2.20355112;
      kfac= 1.0;
      if(MCDataPeriode == "MC16a")
      {
         sum_weights= 6.98404e+06;
      }

      if(MCDataPeriode == "MC16d")
      {
         sum_weights=  3.49307e+06;
      }

      if(MCDataPeriode == "MC16e")
      {
         sum_weights= 5.7981e+06;
      }
   }

   if(sampleName=="363357")
   {  
      xsec= 6.7975;
      kfac= 1.0;
      if(MCDataPeriode == "MC16a")
      {
         sum_weights= 559370;
      }

      if(MCDataPeriode == "MC16d")
      {
         sum_weights= 555940;
      }

      if(MCDataPeriode == "MC16e")
      {
         sum_weights= 928257;
      }
   }

   if(sampleName=="363358")
   {  
      xsec= 3.4328;
      kfac= 1.0;
      if(MCDataPeriode == "MC16a")
      {
         sum_weights= 254404;
      }

      if(MCDataPeriode == "MC16d")
      {
         sum_weights= 1.26717e+06;
      }

      if(MCDataPeriode == "MC16e")
      {
         sum_weights= 421819;
      }
   }

   if(sampleName=="363359")
   {  
      xsec= 24.708;
      kfac= 1.0;
      if(MCDataPeriode == "MC16a")
      {
         sum_weights= 1.07634e+06;
      }

      if(MCDataPeriode == "MC16d")
      {
         sum_weights= 5.37057e+06;
      }

      if(MCDataPeriode == "MC16e")
      {
         sum_weights= 1.78463e+06;
      }
   }

   if(sampleName=="363360")
   {  
      xsec= 24.724;
      kfac= 1.0;
      if(MCDataPeriode == "MC16a")
      {
         sum_weights= 1.08043e+06;
      }

      if(MCDataPeriode == "MC16d")
      {
         sum_weights= 2.14992e+06;
      }

      if(MCDataPeriode == "MC16e")
      {
         sum_weights= 1.78713e+06;
      }
   }

   if(sampleName=="363489")
   {  
      xsec= 11.42;
      kfac= 1.0;
      if(MCDataPeriode == "MC16a")
      {
         sum_weights= 1.1245e+06;
      }

      if(MCDataPeriode == "MC16d")
      {
         sum_weights= 5.62809e+06;
      }

      if(MCDataPeriode == "MC16e")
      {
         sum_weights= 1.87376e+06;
      }
   }

   if(sampleName=="363494")
   {  
      xsec= 0.60293;
      kfac= 1.0;
      if(MCDataPeriode == "MC16a")
      {
         sum_weights= 1.06154e+06;
      }

      if(MCDataPeriode == "MC16d")
      {
         sum_weights= 1.07094e+06;
      }

      if(MCDataPeriode == "MC16e")
      {
         sum_weights= 1.76131e+06;
      }
   }

   if(sampleName=="364283")
   {  
      xsec= 0.010567;
      kfac= 1.0;
      if(MCDataPeriode == "MC16a")
      {
         sum_weights= 347963;
      }

      if(MCDataPeriode == "MC16d")
      {
         sum_weights= 427261;
      }

      if(MCDataPeriode == "MC16e")
      {
         sum_weights= 6.06443e+07;
      }
   }

   if(sampleName=="364284")
   {  
      xsec= 0.047066;
      kfac= 1.0;
      if(MCDataPeriode == "MC16a")
      {
         sum_weights= 1.7298e+06;
      }

      if(MCDataPeriode == "MC16d")
      {
         sum_weights= 2.21104e+06;
      }

      if(MCDataPeriode == "MC16e")
      {
         sum_weights= 2.8626e+06;
      }
   }

   if(sampleName=="364285")
   {  
      xsec= 0.11629;
      kfac= 1.0;
      if(MCDataPeriode == "MC16a")
      {
         sum_weights= 302610;
      }

      if(MCDataPeriode == "MC16d")
      {
         sum_weights= 383024;
      }

      if(MCDataPeriode == "MC16e")
      {
         sum_weights= 605580;
      }
   }

   if(sampleName=="364287")
   {  
      xsec= 0.040854;
      kfac= 1.0;
      if(MCDataPeriode == "MC16a")
      {
         sum_weights= 100818;
      }

      if(MCDataPeriode == "MC16d")
      {
         sum_weights= 126659;
      }

      if(MCDataPeriode == "MC16e")
      {
         sum_weights= 1.01272e+06;
      }
   }

   if(sampleName=="345705")
   {  
      xsec= 0.0099577;
      kfac= 1.0;
      if(MCDataPeriode == "MC16a")
      {
         sum_weights= 100065;
      }

      if(MCDataPeriode == "MC16d")
      {
         sum_weights= 125057;
      }

      if(MCDataPeriode == "MC16e")
      {
         sum_weights= 500338;
      }
   }

   if(sampleName=="345706")
   {  
      xsec= 0.01008;
      kfac= 1.0;
      if(MCDataPeriode == "MC16a")
      {
         sum_weights= 1.17615e+06;
      }

      if(MCDataPeriode == "MC16d")
      {
         sum_weights= 1.52166e+06;
      }

      if(MCDataPeriode == "MC16e")
      {
         sum_weights= 1.66958e+06;
      }
   }

   if(sampleName=="345723")
   {  
      xsec= 0.0071139;
      kfac= 1.0;
      if(MCDataPeriode == "MC16a")
      {
         sum_weights= 1.47663e+06;
      }

      if(MCDataPeriode == "MC16d")
      {
         sum_weights= 1.79005e+06;
      }

      if(MCDataPeriode == "MC16e")
      {
         sum_weights= 3.00737e+06;
      }
   }

   if(sampleName=="345723")
   {  
      xsec= 0.0071139;
      kfac= 1.0;
      if(MCDataPeriode == "MC16a")
      {
         sum_weights= 1.47663e+06;
      }

      if(MCDataPeriode == "MC16d")
      {
         sum_weights= 1.79005e+06;
      }

      if(MCDataPeriode == "MC16e")
      {
         sum_weights= 3.00737e+06;
      }
   }

   if(sampleName=="413008")
   {  
      xsec= 0.65206;
      kfac= 1.0;
      if(MCDataPeriode == "MC16a")
      {
         sum_weights= 6.13296e+06;
      }

      if(MCDataPeriode == "MC16d")
      {
         sum_weights= 7.4467e+06;
      }

      if(MCDataPeriode == "MC16e")
      {
         sum_weights= 9.97151e+06;
      }
   }

   TString sampN_wj[42]={"364156","364157","364158","364159","364160","364161","364162","364163","364164","364165","364166","364167","364168","364169","364170",
   "364171","364172","364173","364174","364175","364176","364177","364178","364179","364180","364181","364182","364183","364184","364185","364186","364187",
   "364188","364189","364190","364191","364192","364193","364194","364195","364196","364197"};

   float xs_wj[42]={15792.87215,2493.0619,865.187028,637.8999768,230.4464484,79.77068832,203.8090145,99.411888,37.0138399,39.485424,23.08497526,9.46691631,15.01,
   1.2347,15796.61228,2493.25335,843.287436,638.1610689,228.431016,78.82109739,203.4220793,97.87939,37.6682241,39.4167237,23.05124745,9.99347605,15.05,1.2344,15795.213,
   2484.84842,863.5297,638.833848,229.2026793,79.8780271,203.4199596,96.7796352,36.272676,40.48765416,22.9381737,9.78222568,15.053,1.2343};

   float sum_wgt_MC16a_wj[42]={1.67834e+07,4.85891e+07,4.08141e+07,5.48221e+06,3.73974e+06,8.10169e+06,6.22887e+06,9.5123e+06,1.85316e+07,4.37683e+06,2.81315e+06,
   5.69031e+06,6.02934e+06,4.10815e+06,1.67884e+07,4.85211e+07,4.98374e+07,5.41599e+06,3.76077e+06,8.02989e+06,6.2347e+06,9.52851e+06,1.84335e+07,4.37841e+06,
   2.8125e+06,5.7354e+06,6.08545e+06,4.12914e+06,1.68192e+07,1.14651e+07,1.06269e+07,5.49649e+06,3.77248e+06,4.02645e+06,1.23937e+07,8.0115e+06,1.85371e+07,
   6.56975e+06,4.21468e+06,2.87229e+06,5.99811e+06,4.11312e+06};

   float sum_wgt_MC16d_wj[42]={2.09804e+07,2.54604e+07,6.60615e+07,6.80909e+06,4.73603e+06,1.02516e+07,7.67107e+06,1.19659e+07,1.95954e+07,5.45743e+06,3.51151e+06,
   7.22723e+06,7.53773e+06,5.13978e+06,2.07328e+07,3.14738e+07,5.40032e+07,6.77904e+06,4.6862e+06,1.00541e+07,7.73376e+06,1.19755e+07,2.36227e+07,5.25905e+06,
   3.50755e+06,7.16474e+06,7.52202e+06,9.18131e+06,2.09912e+07,1.42635e+07,1.32644e+07,6.799e+06,4.7043e+06,5.03896e+06,1.55691e+07,9.98573e+06,2.36452e+07,
   8.18029e+06,5.27427e+06,3.58656e+06,7.5463e+06,5.12181e+06};

   float sum_wgt_MC16e_wj[42]={2.78914e+07,7.88578e+07,4.47266e+07,9.0772e+06,6.02805e+06,1.34349e+07,1.04398e+07,1.59184e+07,3.07953e+07,7.27782e+06,4.67417e+06,
   9.54768e+06,9.99165e+06,6.82858e+06,2.7912e+07,7.89511e+07,7.84196e+07,9.01238e+06,6.27807e+06,1.24914e+07,1.04216e+07,1.58714e+07,3.08399e+07,7.26277e+06,
   4.66954e+06,9.54417e+06,9.98964e+06,6.85755e+06,2.7945e+07,1.82907e+07,1.76446e+07,9.11135e+06,6.24883e+06,1.00995e+07,2.06682e+07,1.32233e+07,3.09107e+07,
   1.09217e+07,7.01772e+06,4.77356e+06,1.00279e+07,6.81892e+06};

   for(int i =0; i<42; i++)
   {
     if(sampleName==sampN_wj[i] && MCDataPeriode == "MC16a")
     {
        xsec=xs_wj[i];
        kfac=0.9702;
        sum_weights=sum_wgt_MC16a_wj[i];
     }

     if(sampleName==sampN_wj[i] && MCDataPeriode == "MC16d")
     {
        xsec=xs_wj[i];
        kfac=0.9702;
        sum_weights=sum_wgt_MC16d_wj[i];
     }

     if(sampleName==sampN_wj[i] && MCDataPeriode == "MC16e")
     {
        xsec=xs_wj[i];
        kfac=0.9702;
        sum_weights=sum_wgt_MC16e_wj[i];
     }
   }



   TString sampN_zj[60]={"364100","364101","364102","364103","364104","364105","364106","364107","364108","364109","364110","364111","364112","364113","364114",
   "364115","364116","364117","364118","364119","364120","364121","364122","364123","364124","364125","364126","364127","364128","364129","364130","364131","364132",
   "364133","364134","364135","364136","364137","364138","364139","364140","364141","364198","364199","364200","364201","364202","364203","364204","364205","364206",
   "364207","364208","364209","364210","364211","364212","364213","364214","364215"};

   float xs_zj[60]={1630.2243,223.717472,127.1799342,75.0164716, 20.3477432, 12.3885125, 24.28530322, 9.2754186, 6.01361075, 4.77297475, 2.265570784, 1.491320988, 1.7881,
   0.14769, 1627.176708, 223.73136, 126.4502953, 76.292515, 20.3360066, 12.6227733, 25.0300, 9.3719948, 6.08263, 4.869231562, 2.279979034, 1.494370818, 1.8081, 0.14857, 1627.725872,
   223.881432, 127.7329554, 76.0261671, 20.212279, 12.29393, 24.80341201, 9.3282378, 5.47909362, 4.791190, 2.2756252, 1.5028376, 1.8096, 0.14834, 2330.668648, 83.157119, 44.9477098,
   5.6385148, 2.765914454, 0.525893951, 2331.24756, 83.808188, 44.9761928, 5.4876108, 2.781596415, 0.499306626, 2332.26144, 81.10263, 44.9908056, 5.5434704, 2.793550, 0.4697209};

   float sum_wgt_MC16a_zj[60]={5.37248e+06,2.87199e+06,4.12099e+06,2.16649e+06,732466,2.08226e+06,2.97401e+06,1.99109e+06,8.64667e+06,1.72841e+06,918653,3.74946e+06,2.98051e+06,
   1.01705e+06,5.37483e+06,2.87288e+06,4.11018e+06,2.17538e+06,724682,2.08382e+06,2.99657e+06,2.0014e+06,8.62671e+06,1.7234e+06,918310,3.74627e+06,2.96847e+06,1.01652e+06,
   5.37604e+06,2.86944e+06,4.11444e+06,2.19096e+06,731614,2.09076e+06,2.95972e+06,2.00116e+06,3.47263e+06,6.85424e+06,918260,1.86088e+06,2.97848e+06,1.01827e+06,3.34674e+06,
   633230,1.33863e+06,640180,575003,246688,3.35077e+06,634977,1.35566e+06,641174,575349,246286,3.35083e+06,629986,1.35244e+06,642792,577537,246056};

   float sum_wgt_MC16d_zj[60]={6.69374e+06,3.57621e+06,4.96649e+06,2.7176e+06,902414,2.60676e+06,3.71602e+06,2.49178e+06,1.08331e+07,2.11655e+06,1.14598e+06,4.68323e+06,
   3.72533e+06,1.27407e+06,6.71901e+06,3.5837e+06,5.1392e+06,2.70266e+06,907295,2.62872e+06,3.74416e+06,2.49978e+06,1.08892e+07,2.15262e+06,1.14707e+06,4.68178e+06,
   3.67771e+06,1.27033e+06,6.72628e+06,3.53296e+06,5.13739e+06,2.7557e+06,903050,2.62897e+06,3.73485e+06,2.50053e+06,4.32478e+06,8.62032e+06,1.13385e+06,2.34316e+06,
   3.72324e+06,1.2729e+06,7.53976e+06,1.42672e+06,3.04339e+06,1.44085e+06,1.28352e+06,300008,7.53069e+06,1.4314e+06,3.04508e+06,1.28158e+06,1.15117e+06,492301,
   6.67473e+06,1.26132e+06,2.70857e+06,1.28318e+06,720358,308227};

   float sum_wgt_MC16e_zj[60]={8.93622e+06,4.77926e+06,6.78255e+06,3.61556e+06,1.19295e+06,3.47067e+06,4.92912e+06,3.32423e+06,1.43671e+07,
   2.86554e+06,1.53185e+06,6.20587e+06,5.07812e+06,1.6227e+06,8.91779e+06,4.7721e+06,6.83013e+06,3.6381e+06,1.20844e+06,3.47114e+06,5.01136e+06,
   3.32398e+06,1.44376e+07,2.95158e+06,1.59351e+06,6.20312e+06,4.93026e+06,1.69792e+06,8.93207e+06,4.77104e+06,6.84357e+06,3.65669e+06,1.20985e+06,
   3.47688e+06,4.9663e+06,3.32968e+06,5.75619e+06,1.1374e+07,1.53285e+06,3.11996e+06,4.92279e+06,1.69785e+06,5.55918e+06,1.0521e+06,2.24288e+06,
   1.07431e+06,957837,410957,5.5568e+06,1.04911e+06,2.25252e+06,1.06597e+06,956297,413827,5.52637e+06,1.05096e+06,2.24459e+06,1.06848e+06,959935,
   411443};

   for(int j =0; j<60; j++)
   {
     if(sampleName==sampN_zj[j] && MCDataPeriode == "MC16a")
     {
        xsec=xs_zj[j];
        kfac=0.9751;
        sum_weights=sum_wgt_MC16a_zj[j];
     }

     if(sampleName==sampN_zj[j] && MCDataPeriode == "MC16d")
     {
        xsec=xs_zj[j];
        kfac=0.9751;
        sum_weights=sum_wgt_MC16d_zj[j];
     }

     if(sampleName==sampN_zj[j] && MCDataPeriode == "MC16e")
     {
        xsec=xs_zj[j];
        kfac=0.9751;
        sum_weights=sum_wgt_MC16e_zj[j];
     }
   }

   TString sampN_Sig[30]={"504555", "504556", "504557", "504558", "504559", "504560", "504561", "504562", "504563", "504564", "504565", "504566", "504567", "504568", "504569", "504570", "504571", "510115", "510116", "510117", "510118", "510119", "510120", "510121", "510122", "510123", "510124", "510214", "510215", "510216"};

   //float xs_Sig[27]={};

   float sum_wgt_MC16a_Sig[30]={180945,119059,60293,40453.3,13236.3,6989.68,3830.32,2204.16,1294.27,797.503,318.669,134.431,59.6516,28.8832,13.7754,2.56634,0.533827,2202.08,1300.65,788.158,309.174,134.13,60.0784,28.1436,13.4706,2.53867,0.526864,26919.7,312.996,13.7178};

   float sum_wgt_MC16d_Sig[30]={220351,144808,73617.7,50000.6,16476.7,8610.21,4795.38,2718.47,1608.21,983.982,389.583,164.421,74.7423,34.9249,17.1466,3.18739,0.66101,2696.29,1582.74,979.833,387,163.437,74.3689,33.7192,17.0696,3.14356,0.651347,33207.4,383.902,11.0361};

   float sum_wgt_MC16e_Sig[30]={290480,189737,95733.5,63578.2,21715.9,11396.4,6190.38,3608.47,2108.85,1285.07,513.961,217.466,97.6722,46.2238,22.437,4.1861,0.855298,3556.13,2104.88,1273.13,507.955,215.72,96.3912,45.7266,22.2017,4.12361,0.859035,43826.5,505.547,22.6789};
    
   for(int k =0; k<30; k++)
   {
     if(sampleName==sampN_Sig[k] && MCDataPeriode == "MC16a")
     {
        xsec=1.0;
        kfac=1.0;
        sum_weights=sum_wgt_MC16a_Sig[k];
     }

     if(sampleName==sampN_Sig[k] && MCDataPeriode == "MC16d")
     {
        xsec=1.0;
        kfac=1.0;
        sum_weights=sum_wgt_MC16d_Sig[k];
     }

     if(sampleName==sampN_Sig[k] && MCDataPeriode == "MC16e")
     {
        xsec=1.0;
        kfac=1.0;
        sum_weights=sum_wgt_MC16e_Sig[k];
     }
   }
    
    TString sampN_wj_sh2211[12]={"700338","700339","700340","700341","700342","700343","700344","700345","700346","700347","700348","700349"};

   float xs_wj_sh2211[12]={21742.0,21742.0,21742.0,21806.0,21806.0,21806.0,7680.0,7680.0,7680.0,14126.0,14126.0,14126.0};

   float fe_wj_sh2211[12]={9.376371E-03,0.1489766,8.4360E-01,0.0097968,0.1460112,8.4355E-01,9.011307E-03,0.146033,8.4747E-01,0.00867775,1.4337E-01,8.4746E-01};

   float sum_wgt_MC16a_wj_sh2211[12]={1.17192e+16,2.57379e+16,2.57379e+16,2.01247e+15,1.17326e+16,2.57435e+16,5.15489e+14,2.18739e+15,5.83819e+15,1.22785e+14,5.74906e+14,5.83797e+15};

   float sum_wgt_MC16d_wj_sh2211[12]={2.6351e+15,1.46095e+16,3.21011e+16,2.46549e+15,1.46293e+16,3.21217e+16,6.70385e+14,2.83504e+15,7.4303e+15,1.60273e+14,7.40644e+14,7.42675e+15};

   float sum_wgt_MC16e_wj_sh2211[12]={3.43619e+15,1.97803e+16,4.23578e+16,3.39846e+15,1.97784e+16,4.23246e+16,5.83888e+14,3.73736e+15,9.81265e+15,1.81547e+14,9.89468e+14,9.81737e+15};

   for(int l =0; l<12; l++)
   {
     if(sampleName==sampN_wj_sh2211[l] && MCDataPeriode == "MC16a")
     {
        xsec=xs_wj_sh2211[l]*fe_wj_sh2211[l];
        kfac=1.0;
        sum_weights=sum_wgt_MC16a_wj_sh2211[l];
     }

     if(sampleName==sampN_wj_sh2211[l] && MCDataPeriode == "MC16d")
     {
        xsec=xs_wj_sh2211[l]*fe_wj_sh2211[l];
        kfac=1.0;
        sum_weights=sum_wgt_MC16d_wj_sh2211[l];
     }

     if(sampleName==sampN_wj_sh2211[l] && MCDataPeriode == "MC16e")
     {
        xsec=xs_wj_sh2211[l]*fe_wj_sh2211[l];
        kfac=1.0;
        sum_weights=sum_wgt_MC16e_wj_sh2211[l];
     }
   }



   TString sampN_zj_sh2211[15]={"700320","700321","700322","700323","700324","700325","700326","700327","700328","700329","700330","700331","700332","700333","700334"};

   float xs_zj_sh2211[15]={2221.2,2221.2,2221.2,2221.3,2221.3,2221.3,275.33,275.33,275.33,1013.2,1013.2,1013.2,932.53,932.53,932.53};

   float fe_zj_sh2211[15]={2.493594E-02,0.1298145,8.4607E-01,2.439439E-02,0.13003,8.4639E-01,0.0249068,1.2540E-01,8.5036E-01,2.443101E-02,1.254743E-01,8.5022E-01,2.459687E-02,0.1247522,8.5004E-01};

   float sum_wgt_MC16a_zj_sh2211[15]={2.2988e+14,8.69362e+14,1.19481e+15,2.26063e+14,8.68271e+14,1.19716e+15,5.72805e+13,1.71202e+14,1.73114e+14,1.18746e+14,3.06648e+14,1.70991e+14,1.0948e+14,2.66747e+14,1.08477e+14};

   float sum_wgt_MC16d_zj_sh2211[15]={2.9751e+14,1.12476e+15,1.51247e+15,2.91616e+14,1.12641e+15,1.51413e+15,7.41569e+13,2.21634e+14,2.18157e+14,1.54072e+14,3.95978e+14,2.1447e+14,1.42331e+14,3.44268e+14,1.36328e+14};

   float sum_wgt_MC16e_zj_sh2211[15]={3.90724e+14,1.47423e+15,1.97209e+15,3.84303e+14,1.47564e+15,1.97171e+15,9.67945e+13,2.8986e+14,2.89799e+14,1.70693e+14,5.17215e+14,2.85242e+14,1.86464e+14,4.52134e+14,1.81154e+14};

   for(int m =0; m<15; m++)
   {
     if(sampleName==sampN_zj_sh2211[m] && MCDataPeriode == "MC16a")
     {
        xsec=xs_zj_sh2211[m]*fe_zj_sh2211[m];
        kfac=1.0;
        sum_weights=sum_wgt_MC16a_zj_sh2211[m];
     }

     if(sampleName==sampN_zj_sh2211[m] && MCDataPeriode == "MC16d")
     {
        xsec=xs_zj_sh2211[m]*fe_zj_sh2211[m];
        kfac=1.0;
        sum_weights=sum_wgt_MC16d_zj_sh2211[m];
     }

     if(sampleName==sampN_zj_sh2211[m] && MCDataPeriode == "MC16e")
     {
        xsec=xs_zj_sh2211[m]*fe_zj_sh2211[m];
        kfac=1.0;
        sum_weights=sum_wgt_MC16e_zj_sh2211[m];
     }
   }

    m_norm = xsec*kfac/sum_weights;
   
    std::cout<<"m_norm: "<<m_norm<<std::endl;
   /*
   if(sampleName.Contains("HpWh_M250_L1.root")){
     m_weight_scale = 0.45835*(1/123037);
   }
   if(sampleName.Contains("HpWh_M300_L1.root")){
     m_weight_scale = 0.47379*(1/131096);
   }
   if(sampleName.Contains("HpWh_M350_L1.root")){
     m_weight_scale = 0.49374*(1/43684.1);
   }
   if(sampleName.Contains("HpWh_M400_L1.root")){
     m_weight_scale = 0.50747*(1/41528.7);
   }
   if(sampleName.Contains("HpWh_M500_L1.root")){
     m_weight_scale = 0.53407*(1/13160.5);
   }
   if(sampleName.Contains("HpWh_M600_L1.root")){
     m_weight_scale = 0.56197*(1/7112.87);
   }
   if(sampleName.Contains("HpWh_M700_L1.root")){
     m_weight_scale = 0.58076*(1/3706.68);
   }
   if(sampleName.Contains("HpWh_M800_L1.root")){
     m_weight_scale = 0.60086*(1/1841.95);
   }
   if(sampleName.Contains("HpWh_M900_L1.root")){
     m_weight_scale = 0.61551*(1/1269.09);
   }
   if(sampleName.Contains("HpWh_M1000_L1.root")){
     m_weight_scale = 0.63052*(1/822.079);
   }
   if(sampleName.Contains("HpWh_M1200_L1.root")){
     m_weight_scale = 0.65689*(1/304.93);
   } 
   if(sampleName.Contains("HpWh_M1400_L1.root")){
     m_weight_scale = 0.67148*(1/113.376);
   }
   if(sampleName.Contains("HpWh_M1600_L1.root")){
     m_weight_scale = 0.68918*(1/43.5164);
   }
   if(sampleName.Contains("HpWh_M1800_L1.root")){
     m_weight_scale = 0.70206*(1/33.2352);
   }
   if(sampleName.Contains("HpWh_M2000_L1.root")){
     m_weight_scale = 0.71196*(1/15.2037);
   }
   if(sampleName.Contains("HpWh_M2500_L1.root")){
     m_weight_scale = 0.73269*(1/3.2603);
   }
   */
   /*if(sampleName.Contains("HpWh_M3000_L1.root")){
     m_weight_scale = 0.74764;
   }*/

   Init(tree, MCName, ExpUncertaintyName);
   //Init(tree, sampleName, ExpUncertaintyName);
}

EventLoop::~EventLoop()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t EventLoop::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t EventLoop::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void EventLoop::Init(TTree *tree, TString sampleName, TString ExpUncertaintyName){
   
   //initializeMVA_qqbb();
   ///initializeMVA_lvbb();
  //std::cout<<"Passed Init 1"<<std::endl;
   m_NeutrinoBuilder = new NeutrinoBuilder("MeV");
   sampleName.Resize(sampleName.Index(".root"));

   m_UncNames   = {""};   
   //mySel        = {"Merged_LepP_SR","Merged_LepP_CR","Resolved_LepP_SR","Resolved_LepP_CR","Merged_LepN_SR","Merged_LepN_CR","Resolved_LepN_SR","Resolved_LepN_CR"};
   //mySel        = {"Resolved_SR","Resolved_top"};
   mySel          = {"Merged_SR"};
   //double edges[34] = {200,400,450,500,550,600,650,700,750,800,850,900,950,1000,1050,1100,1150,1200,1250,1300,1350,1400,1450,1500,1550,1600,1650,1700,1750,1800,1850,1900,1950,2000};
   //double edges[33] = {400,450,500,550,600,650,700,750,800,850,900,950,1000,1050,1100,1150,1200,1250,1300,1350,1400,1450,1500,1550,1600,1650,1700,1750,1800,1850,1900,1950,2000};
   double edges[40] = {0,50,100,150,200,300,350,400,450,500,550,600,650,700,750,800,850,900,950,1000,1050,1100,1150,1200,1250,1300,1350,1400,1450,1500,1550,1600,1650,1700,1750,1800,1850,1900,1950,2000};

   h_HT                     = new TH1Fs(sampleName+"_HT", 39, edges,    mySel, m_UncNames, ExpUncertaintyName);
   h_HT_all                 = new TH1Fs(sampleName+"_HT_all", 39, edges,    mySel, m_UncNames, ExpUncertaintyName);
   h_HT_5j                  = new TH1Fs(sampleName+"_HT_5j",39, edges,     mySel, m_UncNames, ExpUncertaintyName);
   h_HT_all_5j                = new TH1Fs(sampleName+"_HT_all_5j", 39, edges,    mySel, m_UncNames, ExpUncertaintyName);
   h_HT_6j                  = new TH1Fs(sampleName+"_HT_6j",39, edges,   mySel, m_UncNames, ExpUncertaintyName);
   h_HT_all_6j                = new TH1Fs(sampleName+"_HT_all_6j", 39, edges,    mySel, m_UncNames, ExpUncertaintyName);
   h_HT_7j                  = new TH1Fs(sampleName+"_HT_7j", 39, edges,    mySel, m_UncNames, ExpUncertaintyName);
   h_HT_all_7j                = new TH1Fs(sampleName+"_HT_all_7j", 39, edges,   mySel, m_UncNames, ExpUncertaintyName);
   h_HT_8j                  = new TH1Fs(sampleName+"_HT_8j", 39, edges,    mySel, m_UncNames, ExpUncertaintyName);
   h_HT_all_8j                = new TH1Fs(sampleName+"_HT_all_8j", 39, edges,    mySel, m_UncNames, ExpUncertaintyName);
   h_HT_9j                  = new TH1Fs(sampleName+"_HT_9j", 39, edges,    mySel, m_UncNames, ExpUncertaintyName);
   h_HT_all_9j                = new TH1Fs(sampleName+"_HT_all_9j",39, edges,   mySel, m_UncNames, ExpUncertaintyName);

   h_evw                    = new TH1Fs(sampleName+"_evw", "",20, -1, 1,    mySel, m_UncNames, ExpUncertaintyName);

   h_HT_1b                     = new TH1Fs(sampleName+"_1B_HT", 39, edges,    mySel, m_UncNames, ExpUncertaintyName);
   h_HT_all_1b                 = new TH1Fs(sampleName+"_1B_HT_all", 39, edges,    mySel, m_UncNames, ExpUncertaintyName);
   h_HT_5j_1b                  = new TH1Fs(sampleName+"_HT_5j_1b", 39, edges,    mySel, m_UncNames, ExpUncertaintyName);
   h_HT_all_5j_1b              = new TH1Fs(sampleName+"_HT_all_5j_1b", 39, edges,    mySel, m_UncNames, ExpUncertaintyName);
   h_HT_6j_1b                  = new TH1Fs(sampleName+"_HT_6j_1b", 39, edges,    mySel, m_UncNames, ExpUncertaintyName);
   h_HT_all_6j_1b               = new TH1Fs(sampleName+"_HT_all_6j_1b", 39, edges,    mySel, m_UncNames, ExpUncertaintyName);
   h_HT_7j_1b                  = new TH1Fs(sampleName+"_HT_7j_1b",  39, edges,  mySel, m_UncNames, ExpUncertaintyName);
   h_HT_all_7j_1b               = new TH1Fs(sampleName+"_HT_all_7j_1b", 39, edges,   mySel, m_UncNames, ExpUncertaintyName);
   h_HT_8j_1b                  = new TH1Fs(sampleName+"_HT_8j_1b",  39, edges,   mySel, m_UncNames, ExpUncertaintyName);
   h_HT_all_8j_1b                = new TH1Fs(sampleName+"_HT_all_8j_1b", 39, edges,   mySel, m_UncNames, ExpUncertaintyName);
   h_HT_9j_1b                  = new TH1Fs(sampleName+"_HT_9j_1b", 39, edges,    mySel, m_UncNames, ExpUncertaintyName);
   h_HT_all_9j_1b               = new TH1Fs(sampleName+"_HT_all_9j_1b", 39, edges,  mySel, m_UncNames, ExpUncertaintyName);

   h_HT_1c                     = new TH1Fs(sampleName+"_1C_HT", 39, edges,    mySel, m_UncNames, ExpUncertaintyName);
   h_HT_all_1c                 = new TH1Fs(sampleName+"_1C_HT_all", 39, edges,    mySel, m_UncNames, ExpUncertaintyName);
   h_HT_5j_1c                 = new TH1Fs(sampleName+"_HT_5j_1c",  39, edges,   mySel, m_UncNames, ExpUncertaintyName);
   h_HT_all_5j_1c              = new TH1Fs(sampleName+"_HT_all_5j_1c",  39, edges,  mySel, m_UncNames, ExpUncertaintyName);
   h_HT_6j_1c                  = new TH1Fs(sampleName+"_HT_6j_1c", 39, edges,    mySel, m_UncNames, ExpUncertaintyName);
   h_HT_all_6j_1c               = new TH1Fs(sampleName+"_HT_all_6j_1c",  39, edges,   mySel, m_UncNames, ExpUncertaintyName);
   h_HT_7j_1c                 = new TH1Fs(sampleName+"_HT_7j_1c",  39, edges,   mySel, m_UncNames, ExpUncertaintyName);
   h_HT_all_7j_1c               = new TH1Fs(sampleName+"_HT_all_7j_1c", 39, edges,   mySel, m_UncNames, ExpUncertaintyName);
   h_HT_8j_1c                  = new TH1Fs(sampleName+"_HT_8j_1c", 39, edges, mySel, m_UncNames, ExpUncertaintyName);
   h_HT_all_8j_1c               = new TH1Fs(sampleName+"_HT_all_8j_1c",  39, edges,   mySel, m_UncNames, ExpUncertaintyName);
   h_HT_9j_1c                 = new TH1Fs(sampleName+"_HT_9j_1c", 39, edges,    mySel, m_UncNames, ExpUncertaintyName);
   h_HT_all_9j_1c               = new TH1Fs(sampleName+"_HT_all_9j_1c",39, edges,  mySel, m_UncNames, ExpUncertaintyName);

   h_HT_all_1l                 = new TH1Fs(sampleName+"_1L_HT_all", 39, edges,    mySel, m_UncNames, ExpUncertaintyName);
   h_HT_1l                     = new TH1Fs(sampleName+"_1L_HT",39, edges,    mySel, m_UncNames, ExpUncertaintyName);
   h_HT_5j_1l                 = new TH1Fs(sampleName+"_HT_5j_1l", 39, edges,   mySel, m_UncNames, ExpUncertaintyName);
   h_HT_all_5j_1l             = new TH1Fs(sampleName+"_HT_all_5j_1l",  39, edges,   mySel, m_UncNames, ExpUncertaintyName);
   h_HT_6j_1l                 = new TH1Fs(sampleName+"_HT_6j_1l", 39, edges,  mySel, m_UncNames, ExpUncertaintyName);
   h_HT_all_6j_1l               = new TH1Fs(sampleName+"_HT_all_6j_1l", 39, edges,    mySel, m_UncNames, ExpUncertaintyName);
   h_HT_7j_1l                 = new TH1Fs(sampleName+"_HT_7j_1l", 39, edges,   mySel, m_UncNames, ExpUncertaintyName);
   h_HT_all_7j_1l              = new TH1Fs(sampleName+"_HT_all_7j_1l", 39, edges,  mySel, m_UncNames, ExpUncertaintyName);
   h_HT_8j_1l                  = new TH1Fs(sampleName+"_HT_8j_1l", 39, edges,    mySel, m_UncNames, ExpUncertaintyName);
   h_HT_all_8j_1l               = new TH1Fs(sampleName+"_HT_all_8j_1l",  39, edges,  mySel, m_UncNames, ExpUncertaintyName);
   h_HT_9j_1l                 = new TH1Fs(sampleName+"_HT_9j_1l",  39, edges,  mySel, m_UncNames, ExpUncertaintyName);
   h_HT_all_9j_1l               = new TH1Fs(sampleName+"_HT_all_9j_1l",  39, edges,   mySel, m_UncNames, ExpUncertaintyName);

   h_LJMass                    = new TH1Fs(sampleName+"_LJMass", "", 30, 50, 200,    mySel, m_UncNames, ExpUncertaintyName);
   h_LJPt                      = new TH1Fs(sampleName+"_LJPt", "", 40, 200, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_LJEta                     = new TH1Fs(sampleName+"_LJEta", "",25, -2.5, 2.5,    mySel, m_UncNames, ExpUncertaintyName);
   h_SLJMass                   = new TH1Fs(sampleName+"_SLJMass", "", 30, 50, 200,    mySel, m_UncNames, ExpUncertaintyName);
   h_SLJPt                     = new TH1Fs(sampleName+"_SLJPt", "", 40, 200, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_SLJEta                    = new TH1Fs(sampleName+"_SLJEta", "",25, -2.5, 2.5,    mySel, m_UncNames, ExpUncertaintyName);

   h_LJMass_1b                 = new TH1Fs(sampleName+"_LJMass_1b", "", 30, 50, 200,    mySel, m_UncNames, ExpUncertaintyName);
   h_LJPt_1b                   = new TH1Fs(sampleName+"_LJPt_1b", "", 40, 200, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_LJEta_1b                  = new TH1Fs(sampleName+"_LJEta_1b", "",25, -2.5, 2.5,    mySel, m_UncNames, ExpUncertaintyName);
   h_SLJMass_1b                = new TH1Fs(sampleName+"_SLJMass_1b", "", 30, 50, 200,    mySel, m_UncNames, ExpUncertaintyName);
   h_SLJPt_1b                  = new TH1Fs(sampleName+"_SLJPt_1b", "", 40, 200, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_SLJEta_1b                 = new TH1Fs(sampleName+"_SLJEta_1b", "",25, -2.5, 2.5,    mySel, m_UncNames, ExpUncertaintyName);

   h_LJMass_1c                 = new TH1Fs(sampleName+"_LJMass_1c", "", 30, 50, 200,    mySel, m_UncNames, ExpUncertaintyName);
   h_LJPt_1c                   = new TH1Fs(sampleName+"_LJPt_1c", "", 40, 200, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_LJEta_1c                  = new TH1Fs(sampleName+"_LJEta_1c", "",25, -2.5, 2.5,    mySel, m_UncNames, ExpUncertaintyName);
   h_SLJMass_1c                = new TH1Fs(sampleName+"_SLJMass_1c", "", 30, 50, 200,    mySel, m_UncNames, ExpUncertaintyName);
   h_SLJPt_1c                  = new TH1Fs(sampleName+"_SLJPt_1c", "", 40, 200, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_SLJEta_1c                 = new TH1Fs(sampleName+"_SLJEta_1c", "",25, -2.5, 2.5,    mySel, m_UncNames, ExpUncertaintyName);

   h_LJMass_1c                 = new TH1Fs(sampleName+"_LJMass_1l", "", 30, 50, 200,    mySel, m_UncNames, ExpUncertaintyName);
   h_LJPt_1c                   = new TH1Fs(sampleName+"_LJPt_1l", "", 40, 200, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_LJEta_1c                  = new TH1Fs(sampleName+"_LJEta_1l", "",25, -2.5, 2.5,    mySel, m_UncNames, ExpUncertaintyName);
   h_SLJMass_1c                = new TH1Fs(sampleName+"_SLJMass_1l", "", 30, 50, 200,    mySel, m_UncNames, ExpUncertaintyName);
   h_SLJPt_1c                  = new TH1Fs(sampleName+"_SLJPt_1l", "", 40, 200, 1000,    mySel, m_UncNames, ExpUncertaintyName);
   h_SLJEta_1c                 = new TH1Fs(sampleName+"_SLJEta_1l", "",25, -2.5, 2.5,    mySel, m_UncNames, ExpUncertaintyName);
   //filling reweighting histograms for different kinematic variables

   // Set object pointer
   //mc_generator_weights  = 0;
   el_pt                 = 0;
   el_eta                = 0;
   el_phi                = 0;
   el_e                  = 0;
   //el_cl_eta             = 0;
   el_charge             = 0;
   /*el_topoetcone20       = 0;
   el_ptvarcone20        = 0;
   el_isTight            = 0;
   el_CF                 = 0;
   el_d0sig              = 0;
   el_delta_z0_sintheta                          = 0;
   el_trigMatch_HLT_e60_lhmedium_nod0            = 0;
   el_trigMatch_HLT_e140_lhloose_nod0            = 0;
   el_trigMatch_HLT_e26_lhtight_nod0_ivarloose   = 0;
   el_LHMedium           = 0;
   el_LHTight            = 0;
   el_isPrompt           = 0;
   el_isoFCLoose         = 0;
   el_isoFCTight         = 0;*/
   mu_pt                 = 0;
   mu_eta                = 0;
   mu_phi                = 0;
   mu_e                  = 0;
   mu_charge             = 0;

   /*mu_d0sig              = 0;
   mu_delta_z0_sintheta  = 0;
   mu_trigMatch_HLT_mu50  = 0;
   mu_trigMatch_HLT_mu26_ivarmedium = 0;
   mu_Medium              = 0;
   mu_isoFCTightTrackOnly = 0;*/
   FatJet_Xbb2020v3_Higgs = 0;
   FatJet_Xbb2020v3_QCD = 0;
   FatJet_Xbb2020v3_Top = 0;
   FatJet_M              = 0;
   FatJet_PT             = 0;
   FatJet_Eta            = 0;
   FatJet_Phi            = 0;
   signal_Jet_E          = 0;
   signal_Jet_PT         = 0;
   signal_Jet_Eta        = 0;
   signal_Jet_Phi        = 0;
   signal_Jet_truthflav  = 0;  //only for MC
   signal_Jet_tagWeightBin_DL1r_Continuous = 0;
   

   //only for MC
   
   truth_pt              = 0;
   truth_eta             = 0;
   truth_phi             = 0;
   truth_m               = 0;
   truth_pdgid           = 0; 
   truth_status          = 0;
   truth_tthbb_info      = 0; 

   //only for MC
   //truth_barcode         = 0;
   /*TrackJet_PT           = 0;
   TrackJet_Eta          = 0;
   TrackJet_Phi          = 0;
   TrackJet_M            = 0;
   TrackJet_btagWeight   = 0;*/
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);
   //std::cout<<"Passed Init 5"<<std::endl;
   fChain->SetBranchAddress("nJets", &nJet, &b_nJet);
   fChain->SetBranchAddress("nLJets", &nFatJets, &b_nFatJets);
   fChain->SetBranchAddress("nMuons", &nMuons, &b_nMuons);
   fChain->SetBranchAddress("nElectrons", &nElectrons, &b_nElectrons);
   fChain->SetBranchAddress("nPrimaryVtx", &nPrimaryVtx, &b_nPrimaryVtx);
   fChain->SetBranchAddress("eventNumber", &eventNumber, &b_eventNumber);
   //fChain->SetBranchAddress("nFatJets", &nFatJets, &b_nFatJets);
   fChain->SetBranchAddress("runNumber", &RunNumber, &b_RunNumber);
   fChain->SetBranchAddress("randomRunNumber", &randomRunNumber, &b_randomRunNumber);
   fChain->SetBranchAddress("GenFiltHT", &GenFiltHT, &b_GenFiltHT);
   
   //Below variables only for MC
   
   fChain->SetBranchAddress("weight_pileup", &weight_pileup, &b_weight_pileup);
   fChain->SetBranchAddress("weight_normalise", &weight_normalise, &b_weight_normalise);
   fChain->SetBranchAddress("weight_mc", &weight_mc, &b_weight_mc);
   fChain->SetBranchAddress("weight_leptonSF", &weight_leptonSF, &b_weight_leptonSF);
   fChain->SetBranchAddress("weight_jvt", &weight_jvt, &b_weight_jvt); 
   fChain->SetBranchAddress("weight_bTagSF_DL1r_Continuous", &weight_bTagSF_DL1r_Continuous, &b_weight_bTagSF_DL1r_Continuous); 
   
    
   fChain->SetBranchAddress("nTaus",      &nTaus,      &b_nTaus);
   //fChain->SetBranchAddress("hasBadMuon", &hasBadMuon, &b_hasBadMuon);
   fChain->SetBranchAddress("HLT_mu26_ivarmedium", &HLT_mu26_ivarmedium, &b_HLT_mu26_ivarmedium);
   fChain->SetBranchAddress("HLT_mu50", &HLT_mu50, &b_HLT_mu50);
   fChain->SetBranchAddress("HLT_e60_lhmedium_nod0", &HLT_e60_lhmedium_nod0, &b_HLT_e60_lhmedium_nod0);
   fChain->SetBranchAddress("HLT_e140_lhloose_nod0", &HLT_e140_lhloose_nod0, &b_HLT_e140_lhloose_nod0);
   fChain->SetBranchAddress("HLT_e26_lhtight_nod0_ivarloose", &HLT_e26_lhtight_nod0_ivarloose, &b_HLT_e26_lhtight_nod0_ivarloose);
   fChain->SetBranchAddress("mu", &Mu, &b_Mu);
   //fChain->SetBranchAddress("mu_actual", &ActualMu, &b_ActualMu);
   fChain->SetBranchAddress("met_met", &met, &b_MET);
   fChain->SetBranchAddress("met_phi", &met_phi, &b_MET_phi);

   //fChain->SetBranchAddress("b_mc_generator_weights", &mc_generator_weights,  &b_mc_generator_weights);
   fChain->SetBranchAddress("el_pt",   &el_pt,  &b_el_pt);
   fChain->SetBranchAddress("el_eta",  &el_eta, &b_el_eta);
   fChain->SetBranchAddress("el_phi",  &el_phi, &b_el_phi);
   fChain->SetBranchAddress("el_e",    &el_e,   &b_el_e);
   fChain->SetBranchAddress("el_charge",        &el_charge,       &b_el_charge);
   
   fChain->SetBranchAddress("mu_pt",   &mu_pt,  &b_mu_pt);
   fChain->SetBranchAddress("mu_eta",  &mu_eta, &b_mu_eta);
   fChain->SetBranchAddress("mu_phi",  &mu_phi, &b_mu_phi);
   fChain->SetBranchAddress("mu_e",    &mu_e,   &b_mu_e);
   fChain->SetBranchAddress("mu_charge",    &mu_charge,   &b_mu_charge);

   fChain->SetBranchAddress("ljet_m", &FatJet_M, &b_FatJet_M);
   fChain->SetBranchAddress("ljet_pt", &FatJet_PT, &b_FatJet_PT);
   fChain->SetBranchAddress("ljet_eta", &FatJet_Eta, &b_FatJet_Eta);
   fChain->SetBranchAddress("ljet_phi", &FatJet_Phi, &b_FatJet_Phi);
   fChain->SetBranchAddress("ljet_Xbb2020v3_Higgs", &FatJet_Xbb2020v3_Higgs, &b_FatJet_Xbb2020v3_Higgs);
   fChain->SetBranchAddress("ljet_Xbb2020v3_QCD", &FatJet_Xbb2020v3_QCD, &b_FatJet_Xbb2020v3_QCD);
   fChain->SetBranchAddress("ljet_Xbb2020v3_Top", &FatJet_Xbb2020v3_Top, &b_FatJet_Xbb2020v3_Top);
   fChain->SetBranchAddress("jet_e",   &signal_Jet_E, &b_signal_Jet_E);
   fChain->SetBranchAddress("jet_pt",  &signal_Jet_PT, &b_signal_Jet_PT);
   fChain->SetBranchAddress("jet_eta", &signal_Jet_Eta, &b_signal_Jet_Eta);
   fChain->SetBranchAddress("jet_phi", &signal_Jet_Phi, &b_signal_Jet_Phi);
   fChain->SetBranchAddress("jet_truthflav", &signal_Jet_truthflav, &b_signal_Jet_truthflav); //only for MC
   fChain->SetBranchAddress("jet_tagWeightBin_DL1r_Continuous", &signal_Jet_tagWeightBin_DL1r_Continuous, &b_signal_Jet_tagWeightBin_DL1r_Continuous);

   //Below variables only for MC
   fChain->SetBranchAddress("truth_pt", &truth_pt, &b_truth_pt);
   fChain->SetBranchAddress("truth_eta", &truth_eta, &b_truth_eta);
   fChain->SetBranchAddress("truth_phi", &truth_phi, &b_truth_phi);
   fChain->SetBranchAddress("truth_m", &truth_m, &b_truth_m);
   fChain->SetBranchAddress("truth_pdgid", &truth_pdgid, &b_truth_pdgid);
   fChain->SetBranchAddress("truth_status", &truth_status, &b_truth_status);
   fChain->SetBranchAddress("truth_tthbb_info", &truth_tthbb_info, &b_truth_tthbb_info); 
   fChain->SetBranchAddress("HF_SimpleClassification", &HF_SimpleClassification, &b_HF_SimpleClassification);
   fChain->SetBranchAddress("mcChannelNumber", &mcChannelNumber, &b_mcChannelNumber);
   fChain->SetBranchAddress("HT_all", &HT_all, &b_HT_all);
   fChain->SetBranchAddress("TopHeavyFlavorFilterFlag", &TopHeavyFlavorFilterFlag, &b_TopHeavyFlavorFilterFlag);

   fChain->SetBranchAddress("boosted_ljets_ejets_2015_DL1r", &boosted_ljets_ejets_2015_DL1r, &b_boosted_ljets_ejets_2015_DL1r);
   fChain->SetBranchAddress("boosted_ljets_ejets_2016_DL1r", &boosted_ljets_ejets_2016_DL1r, &b_boosted_ljets_ejets_2016_DL1r);
   fChain->SetBranchAddress("boosted_ljets_ejets_2017_DL1r", &boosted_ljets_ejets_2017_DL1r, &b_boosted_ljets_ejets_2017_DL1r);
   fChain->SetBranchAddress("boosted_ljets_ejets_2018_DL1r", &boosted_ljets_ejets_2018_DL1r, &b_boosted_ljets_ejets_2018_DL1r);
   fChain->SetBranchAddress("boosted_ljets_mujets_2015_DL1r", &boosted_ljets_mujets_2015_DL1r, &b_boosted_ljets_mujets_2015_DL1r);
   fChain->SetBranchAddress("boosted_ljets_mujets_2016_DL1r", &boosted_ljets_mujets_2016_DL1r, &b_boosted_ljets_mujets_2016_DL1r);
   fChain->SetBranchAddress("boosted_ljets_mujets_2017_DL1r", &boosted_ljets_mujets_2017_DL1r, &b_boosted_ljets_mujets_2017_DL1r);
   fChain->SetBranchAddress("boosted_ljets_mujets_2018_DL1r", &boosted_ljets_mujets_2018_DL1r, &b_boosted_ljets_mujets_2018_DL1r);
   fChain->SetBranchAddress("boosted_ljets_mujets_2015_DL1r_HighPtMuon", &boosted_ljets_mujets_2015_DL1r_HighPtMuon, &b_boosted_ljets_mujets_2015_DL1r_HighPtMuon);
   fChain->SetBranchAddress("boosted_ljets_mujets_2016_DL1r_HighPtMuon", &boosted_ljets_mujets_2016_DL1r_HighPtMuon, &b_boosted_ljets_mujets_2016_DL1r_HighPtMuon);
   fChain->SetBranchAddress("boosted_ljets_mujets_2017_DL1r_HighPtMuon", &boosted_ljets_mujets_2017_DL1r_HighPtMuon, &b_boosted_ljets_mujets_2017_DL1r_HighPtMuon);
   fChain->SetBranchAddress("boosted_ljets_mujets_2018_DL1r_HighPtMuon", &boosted_ljets_mujets_2018_DL1r_HighPtMuon, &b_boosted_ljets_mujets_2018_DL1r_HighPtMuon);
   //fChain->SetBranchAddress("truth_barcode", &truth_barcode, &b_truth_barcode);
   /*fChain->SetBranchAddress("TrackJet_PT",&TrackJet_PT, &b_TrackJet_PT);
   fChain->SetBranchAddress("TrackJet_Eta",&TrackJet_Eta, &b_TrackJet_Eta);
   fChain->SetBranchAddress("TrackJet_Phi", &TrackJet_Phi, &b_TrackJet_Phi);
   fChain->SetBranchAddress("TrackJet_M", &TrackJet_M, &b_TrackJet_M);
   fChain->SetBranchAddress("TrackJet_btagWeight", &TrackJet_btagWeight, &b_TrackJet_btagWeight);*/
   Nom = new TTree("Nom","");
   //Nom->Branch("nFJets",&nJets);
   Notify();
   //std::cout<<"Passed Init 6"<<std::endl;
   //Nominal->Branch("MC16_scale",&MC16_scale);
   //std::cout<<"Passed Init 8"<<std::endl;
}

Bool_t EventLoop::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void EventLoop::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t EventLoop::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef EventLoop_cxx
